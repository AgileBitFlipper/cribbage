package com.thirdsonsoftware

import java.lang.reflect.Array
import java.security.InvalidParameterException
import org.junit.rules.ExpectedException

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class PlayerTest extends GroovyTestCase {

    Player player

    // Let's define some cards to be used in our hands and a start
    final Card aceOfClubs         = new Card(0, Face.ACE,   Suit.CLUBS,    Face.ACE.getValue())
    final Card aceOfDiamonds      = new Card(0, Face.ACE,   Suit.DIAMONDS, Face.ACE.getValue())
    final Card aceOfHearts        = new Card(0, Face.ACE,   Suit.HEARTS,   Face.ACE.getValue())

    final Card twoOfDiamonds      = new Card(0, Face.TWO,   Suit.DIAMONDS, Face.TWO.getValue())
    final Card twoOfHearts        = new Card(0, Face.TWO,   Suit.HEARTS,   Face.TWO.getValue())
    final Card twoOfSpades        = new Card(0, Face.TWO,   Suit.SPADES,   Face.TWO.getValue())

    final Card threeOfClubs       = new Card(0, Face.THREE, Suit.CLUBS,    Face.THREE.getValue())
    final Card threeOfHearts      = new Card(0, Face.THREE, Suit.HEARTS,   Face.THREE.getValue())

    final Card fourOfClubs        = new Card(0, Face.FOUR,  Suit.CLUBS,    Face.FOUR.getValue())
    final Card fourOfDiamonds     = new Card(0, Face.FOUR,  Suit.DIAMONDS, Face.FOUR.getValue())
    final Card fourOfHearts       = new Card(0, Face.FOUR,  Suit.HEARTS,   Face.FOUR.getValue())
    final Card fourOfSpades       = new Card(0, Face.FOUR,  Suit.SPADES,   Face.FOUR.getValue())

    final Card fiveOfHearts       = new Card(0, Face.FIVE,  Suit.HEARTS,   Face.FIVE.getValue())

    final Card sixOfHearts        = new Card(0, Face.SIX,   Suit.HEARTS,   Face.SIX.getValue())
    final Card sixOfSpades        = new Card(0, Face.SIX,   Suit.SPADES,   Face.SIX.getValue())

    final Card sevenOfHearts      = new Card(0, Face.SEVEN, Suit.HEARTS,   Face.SEVEN.getValue())

    final Card eightOfClubs       = new Card(0, Face.EIGHT, Suit.CLUBS,    Face.EIGHT.getValue())
    final Card eightOfHearts      = new Card(0, Face.EIGHT, Suit.HEARTS,   Face.EIGHT.getValue())
    final Card eightOfDiamonds    = new Card(0, Face.EIGHT, Suit.DIAMONDS, Face.EIGHT.getValue())
    final Card eightOfSpades      = new Card(0, Face.EIGHT, Suit.SPADES,   Face.EIGHT.getValue())

    final Card nineOfClubs        = new Card(0, Face.NINE,  Suit.CLUBS,    Face.NINE.getValue())
    final Card nineOfHearts       = new Card(0, Face.NINE,  Suit.HEARTS,   Face.NINE.getValue())

    final Card tenOfClubs         = new Card(0, Face.TEN,   Suit.CLUBS,    Face.TEN.getValue())

    final Card jackOfClubs        = new Card(0, Face.JACK,  Suit.CLUBS,    Face.JACK.getValue())
    final Card jackOfHearts       = new Card(0, Face.JACK,  Suit.HEARTS,   Face.JACK.getValue())

    final Card queenOfClubs       = new Card(0, Face.QUEEN, Suit.CLUBS,    Face.QUEEN.getValue())

    final Card kingOfClubs        = new Card(0, Face.KING,  Suit.CLUBS,    Face.KING.getValue())

    // Let's setup our hands for testing
    ArrayList<Card> handAceTwoEights            = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handSevenAndEights          = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handAceSixesAndEights       = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handFiveSixSevenAndEights   = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handRunOf5AndFlush          = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handAceTwoThreeRuns         = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handRoyalStraight           = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)
    ArrayList<Card> handTwoStraights            = new ArrayList<Card>(Game.CARDS_IN_HAND - 1)

    // Simple arrays for onTable uses
    final ArrayList<Card> arrayAceTwoFourNotSameSuit = new ArrayList<Card>(Arrays.asList(aceOfClubs,twoOfDiamonds,fourOfHearts))


    final int TEST_WON_A_GAME_COUNT = 9

    final String DEFAULT_PLAYER_NAME = "Fred"
    final String TEST_PLAYER_NAME = "Pippy Longstockings"

    final String EMPTY_ARRAY = "[]"

    final String HAND_ROYAL_STRAIGHT = "[9♣(0)-9, T♣(0)-10, J♣(0)-10, Q♣(0)-10, K♣(0)-10]"
    final String HAND_ROYAL_STRAIGHT_MISSING_NINE = "[T♣(0)-10, J♣(0)-10, Q♣(0)-10, K♣(0)-10]"
    final String NINE_OF_CLUBS = "9♣(0)-9"
    final String NINE_OF_CLUBS_ARRAY = "[9♣(0)-9]"

    ArrayList<Choice> choices

    void setUp() {

        super.setUp()

        player = new Player(DEFAULT_PLAYER_NAME)

        // Sevens and Eights will help test for Double Pair Royale and 15-2s
        handSevenAndEights.add(eightOfClubs)
        handSevenAndEights.add(eightOfDiamonds)
        handSevenAndEights.add(eightOfHearts)
        handSevenAndEights.add(eightOfSpades)
        handSevenAndEights.add(sevenOfHearts)

        // Ace, Sixes and Eights will help test for multiple-card 15-s
        handAceSixesAndEights.add(aceOfHearts)
        handAceSixesAndEights.add(sixOfHearts)
        handAceSixesAndEights.add(sixOfSpades)
        handAceSixesAndEights.add(eightOfClubs)
        handAceSixesAndEights.add(eightOfDiamonds)

        // 5, 6, 7, 8, 8 will test for 2 4-card runs and 2 15-2s
        handFiveSixSevenAndEights.add(fiveOfHearts)
        handFiveSixSevenAndEights.add(sixOfSpades)
        handFiveSixSevenAndEights.add(sevenOfHearts)
        handFiveSixSevenAndEights.add(eightOfDiamonds)
        handFiveSixSevenAndEights.add(eightOfClubs)

        // A, 2, 8, 8, 8 will test for Pair Royale
        handAceTwoEights.add(aceOfHearts)
        handAceTwoEights.add(twoOfHearts)
        handAceTwoEights.add(eightOfDiamonds)
        handAceTwoEights.add(eightOfHearts)
        handAceTwoEights.add(eightOfSpades)

        // Run and Flush
        handRunOf5AndFlush.add(fiveOfHearts)
        handRunOf5AndFlush.add(sixOfHearts)
        handRunOf5AndFlush.add(sevenOfHearts)
        handRunOf5AndFlush.add(eightOfHearts)
        handRunOf5AndFlush.add(nineOfHearts)

        // Ace, Two, Three, Three for two runs of three
        handAceTwoThreeRuns.add(aceOfHearts)
        handAceTwoThreeRuns.add(twoOfHearts)
        handAceTwoThreeRuns.add(twoOfDiamonds)
        handAceTwoThreeRuns.add(threeOfClubs)
        handAceTwoThreeRuns.add(fiveOfHearts)

        // Run of 5 with 9-K of Clubs
        handRoyalStraight.add(nineOfClubs)
        handRoyalStraight.add(tenOfClubs)
        handRoyalStraight.add(jackOfClubs)
        handRoyalStraight.add(queenOfClubs)
        handRoyalStraight.add(kingOfClubs)

        // Run of A,2,3 and J,Q
        handTwoStraights.add(aceOfHearts)
        handTwoStraights.add(twoOfDiamonds)
        handTwoStraights.add(threeOfClubs)
        handTwoStraights.add(jackOfHearts)
        handTwoStraights.add(queenOfClubs)

        // Deal with choices
        choices = new ArrayList<Choice>()
    }

    void tearDown() {
    }

    void testReset() {
        player.hand = handRoyalStraight
        player.played.add(player.hand.remove(0))
        player.starts = true
        assertEquals(HAND_ROYAL_STRAIGHT_MISSING_NINE, player.hand.toString())
        assertEquals(NINE_OF_CLUBS_ARRAY, player.played.toString())
        assertEquals(true, player.starts)
        player.reset()
        assertEquals(EMPTY_ARRAY, player.hand.toString())
        assertEquals(EMPTY_ARRAY, player.played.toString())
        assertEquals(false, player.starts)
    }

    void testSetWonAGameCount() {
        assertEquals(0, player.getWonAGameCount())
        player.setWonAGameCount(TEST_WON_A_GAME_COUNT)
        assertEquals(TEST_WON_A_GAME_COUNT,player.getWonAGameCount())
    }

    void testGetWonAGameCount() {
        assertEquals(0,player.getWonAGameCount())
    }

    void testGetScore() {
        assertEquals(0,player.getScore())
    }

    void testSetScore() {
        assertEquals(0,player.getScore())
        player.setScore(Game.POINTS_FOR_WIN);
        assertEquals(Game.POINTS_FOR_WIN,player.getScore())
    }

    void testGetName() {
        assertEquals(DEFAULT_PLAYER_NAME,player.getName())
    }

    void testSetName() {
        assertEquals(DEFAULT_PLAYER_NAME,player.getName())
        player.setName(TEST_PLAYER_NAME)
        assertEquals(TEST_PLAYER_NAME,player.getName())
    }

    void testGetDealer() {
        assertEquals(false, player.getDealer());
    }

    void testSetDealer() {
        assertEquals(false, player.getDealer());
        player.setDealer(true);
        assertEquals(true, player.getDealer());
    }

    void testGetHand() {
        assertEquals(EMPTY_ARRAY, player.getHand().toString())
        player.setHand(handRoyalStraight)
        assertEquals(HAND_ROYAL_STRAIGHT,player.getHand().toString())
    }

    void testGetPlayed() {
        assertEquals(EMPTY_ARRAY,player.getPlayed().toString())
        player.played.add(nineOfClubs)
        assertEquals(NINE_OF_CLUBS_ARRAY,player.getPlayed().toString())
    }

    void testHasWon() {
        assertEquals(false,player.hasWon())
        player.score = Game.POINTS_FOR_WIN
        assertEquals(true,player.hasWon())
    }

    void testHasAnEmptyHand() {
        assertEquals(true,player.hasAnEmptyHand())
        player.setHand(handRoyalStraight)
        assertEquals(false,player.hasAnEmptyHand())
    }

    void testPlayACardNoPlayerList() {
        try {
            player.setHand(handAceTwoThreeRuns)
            Round round = new Round(0, null)
            player.playACard(arrayAceTwoFourNotSameSuit, round)

            fail("An invalid exception should have been thrown for null player list.")
        } catch ( IllegalArgumentException iae ) {
            assertEquals("A Round requires a list of players to be provided.", iae.getMessage())
        }
    }

    void testPlayACardSinglePlayer() {
        try {
            player.setHand(handAceTwoThreeRuns)
            Round round = new Round(0, new ArrayList<Player>(Arrays.asList(player)))
            player.playACard(arrayAceTwoFourNotSameSuit, round)

            fail("An invalid exception should have been thrown for ot enough players.")
        } catch (IllegalArgumentException iae) {
            assertEquals("A Round requires that at least two players.", iae.getMessage())
        }
    }

    void testPlayACard() {
        Player playerGeorge = new Player("George");
        Round round = new Round(0,new ArrayList<Player>(Arrays.asList(player,playerGeorge)))
        player.setHand(handAceTwoThreeRuns)
        assertEquals(threeOfClubs, player.playACard(arrayAceTwoFourNotSameSuit,round))
    }

    void testGenerateChoiceWithRun() {
        player.generateChoice(choices,arrayAceTwoFourNotSameSuit,threeOfClubs)
        assertEquals(choices.size(),1)
        assertEquals(choices.get(0).card,threeOfClubs)
        assertEquals(choices.get(0).score,4)
    }

    void testGenerateChoiceWith152() {
        player.generateChoice(choices,arrayAceTwoFourNotSameSuit,eightOfSpades)
        assertEquals(choices.size(),1)
        assertEquals(choices.get(0).card,eightOfSpades)
        assertEquals(choices.get(0).score,2)
    }

    void testCountRunsOnTable() {
        /**
         *
         * Let's use the following gameplay to prove this method works properly.
         *
         *   Sample Gameplay
         *   ===============
         *   Player 1: A     1   -   0
         *   Player 2: 3     4   -   0
         *   Player 1: 4     8   -   0
         *   Player 2: 2     10  -   4
         *   Player 1: 5     15  -   2 + 5
         *   Player 2: 2     17  -   0
         *   Player 1: 4     21  -   0
         *   Player 2: 3     24  -   4
         *   Player 1: 2     26  -   3
         *   Player 2: A     27  -   4
         *   Player 1: 4     31  -   2 + 4
         *
         */
        ArrayList<Card> onTable = new ArrayList<Card>()
        assertEquals(0,player.countRunsOnTable(onTable, aceOfHearts))
        onTable.add(aceOfHearts)
        assertEquals(0,player.countRunsOnTable(onTable, threeOfClubs))
        onTable.add(threeOfClubs)
        assertEquals(0,player.countRunsOnTable(onTable, fourOfSpades))
        onTable.add(fourOfSpades)
        assertEquals(4,player.countRunsOnTable(onTable, twoOfDiamonds))
        onTable.add(twoOfDiamonds)
        assertEquals(5,player.countRunsOnTable(onTable, fiveOfHearts))
        onTable.add(fiveOfHearts)
        assertEquals(0,player.countRunsOnTable(onTable, twoOfHearts))
        onTable.add(twoOfHearts)
        assertEquals(0,player.countRunsOnTable(onTable, fourOfHearts))
        onTable.add(fourOfHearts)
        assertEquals(4,player.countRunsOnTable(onTable, threeOfHearts))
        onTable.add(threeOfHearts)
        assertEquals(3,player.countRunsOnTable(onTable, twoOfSpades))
        onTable.add(twoOfSpades)
        assertEquals(4,player.countRunsOnTable(onTable, aceOfClubs))
        onTable.add(aceOfClubs)
        assertEquals(4,player.countRunsOnTable(onTable, fourOfDiamonds))
    }

    void testCountPairsOnTable() {
        /**
         *
         * Let's use the following gameplay to prove this method works properly.
         *
         *   Sample Gameplay
         *   ===============
         *   Player 1: A     1   -   0
         *   Player 2: 3     4   -   0
         *   Player 1: 3     7   -   2
         *   Player 2: A     8   -   0
         *   Player 1: 4     12  -   0
         *   Player 2: 4     16  -   2
         *   Player 1: 4     20  -   6
         *   Player 2: 4     24  -   12
         *   Player 1: 2     26  -   0
         *   Player 2: 2     28  -   2
         *   Player 1: 2     30  -   6
         *   Player 2: A     31  -   0
         *
         */
        ArrayList<Card> onTable = new ArrayList<Card>()
        assertEquals(0,player.countPairsOnTable(onTable, aceOfHearts))
        onTable.add(aceOfHearts)
        assertEquals(0,player.countPairsOnTable(onTable, threeOfClubs))
        onTable.add(threeOfClubs)
        assertEquals(2,player.countPairsOnTable(onTable, threeOfHearts))
        onTable.add(threeOfHearts)
        assertEquals(0,player.countPairsOnTable(onTable, aceOfClubs))
        onTable.add(aceOfClubs)
        assertEquals(0,player.countPairsOnTable(onTable, fourOfSpades))
        onTable.add(fourOfSpades)
        assertEquals(2,player.countPairsOnTable(onTable, fourOfHearts))
        onTable.add(fourOfHearts)
        assertEquals(6,player.countPairsOnTable(onTable, fourOfDiamonds))
        onTable.add(fourOfDiamonds)
        assertEquals(12,player.countPairsOnTable(onTable, fourOfClubs))
        onTable.add(fourOfClubs)
        assertEquals(0,player.countPairsOnTable(onTable, twoOfDiamonds))
        onTable.add(twoOfDiamonds)
        assertEquals(2,player.countPairsOnTable(onTable, twoOfHearts))
        onTable.add(twoOfHearts)
        assertEquals(6,player.countPairsOnTable(onTable, twoOfSpades))
        onTable.add(twoOfSpades)
        assertEquals(0,player.countPairsOnTable(onTable, aceOfDiamonds))
    }

    void testCount15sAnd31sOnTable() {
        /**
         *
         * Let's use the following gameplay to prove this method works properly.
         *
         *   Sample Gameplay
         *   ===============
         *   Player 1: A     1   -   0
         *   Player 2: K     11  -   0
         *   Player 1: 4     15  -   2
         *   Player 2: J     25  -   0
         *   Player 1: 6     31  -   2
         *
         */
        ArrayList<Card> onTable = new ArrayList<Card>()
        assertEquals(0,player.count15sAnd31sOnTable(onTable, aceOfHearts))
        onTable.add(aceOfHearts)
        assertEquals(0,player.count15sAnd31sOnTable(onTable, kingOfClubs))
        onTable.add(kingOfClubs)
        assertEquals(2,player.count15sAnd31sOnTable(onTable, fourOfSpades))
        onTable.add(fourOfSpades)
        assertEquals(0,player.count15sAnd31sOnTable(onTable, jackOfHearts))
        onTable.add(jackOfHearts)
        assertEquals(2,player.count15sAnd31sOnTable(onTable, sixOfSpades))
    }

    void testGetLargestValuedCard() {
        player.hand = handRoyalStraight
        assertTrue(kingOfClubs == player.getLargestValuedCard())
        player.hand = handAceTwoThreeRuns
        assertEquals(fiveOfHearts, player.getLargestValuedCard())
    }

    void testDisplayChoicesFull() {
        ArrayList<Choice> testChoices = new ArrayList<Choice>()
        testChoices.add(new Choice(aceOfHearts))
        testChoices.add(new Choice(twoOfDiamonds))
        testChoices.add(new Choice(threeOfClubs))
        testChoices.add(new Choice(fiveOfHearts))
        testChoices.add(new Choice(sixOfSpades))

        String str = player.displayChoices( "Test choices", testChoices )
        assertEquals("Test choices (5):\n  [A♥(0)-1, 2♦(0)-2, 3♣(0)-3, 5♥(0)-5, 6♠(0)-6]", str)
    }

    void testDisplayChoicesEmpty() {
        ArrayList<Choice> testChoices = new ArrayList<Choice>()
        String str = player.displayChoices( "Empty choices", testChoices )
        assertEquals("Empty choices (0):\n  [<empty>]", str)
    }

    /**
     * todo: we need to figure out why this throws some reflective problmes
     */
    void testDisplayCardsAsCards() {
        String asCards = "Test hand: (5):\n" +
                "       -----  -----  -----  -----  ----- \n" +
                "      |9♣   ||T♣   ||J♣   ||Q♣   ||K♣   |\n" +
                "      | ( 0)|| ( 0)|| ( 0)|| ( 0)|| ( 0)|\n" +
                "      |   ♣9||   ♣T||   ♣J||   ♣Q||   ♣K|\n" +
                "       -----  -----  -----  -----  ----- \n"
        assertEquals(asCards, Player.displayCards(true,"Test hand:",handRoyalStraight))
    }

    /**
     * todo: we need to understand why this throws some reflective problems
     */
    void testDisplayCardsAsString() {
        String asString = "Test hand: (5):\n"+
                "  [9♣(0)-9, T♣(0)-10, J♣(0)-10, Q♣(0)-10, K♣(0)-10]\n"
        assertEquals(asString, Player.displayCards(false,"Test hand:",handRoyalStraight))
    }

    void testShowCardClubQueen() {
        assertEquals(
                " ----- \n" +
                "|Q♣   |\n" +
                "| ( 0)|\n" +
                "|   ♣Q|\n" +
                " ----- \n",player.showCard(queenOfClubs))
    }

    void testShowCardHeartAce() {
        assertEquals(
                " ----- \n" +
                "|A♥   |\n" +
                "| ( 0)|\n" +
                "|   ♥A|\n" +
                " ----- \n",player.showCard(aceOfHearts))
    }

    void testShowCardDiamondFour() {
        assertEquals(
                " ----- \n" +
                "|4♦   |\n" +
                "| ( 0)|\n" +
                "|   ♦4|\n" +
                " ----- \n",player.showCard(fourOfDiamonds))
    }

    void testShowCardSpadesEight() {
        assertEquals(
                " ----- \n" +
                "|8♠   |\n" +
                "| ( 0)|\n" +
                "|   ♠8|\n" +
                " ----- \n",player.showCard(eightOfSpades))
    }

    void testToString() {
        player.setHand(handRoyalStraight)
        assertEquals(
                "  Name: Fred\n"+
                "    Starts: no\n"+
                "    Score: 0\n"+
                "    Hand (5):\n"+
                "       -----  -----  -----  -----  ----- \n" +
                "      |9♣   ||T♣   ||J♣   ||Q♣   ||K♣   |\n" +
                "      | ( 0)|| ( 0)|| ( 0)|| ( 0)|| ( 0)|\n" +
                "      |   ♣9||   ♣T||   ♣J||   ♣Q||   ♣K|\n" +
                "       -----  -----  -----  -----  ----- \n",player.toString())
    }

    void testHashCode() {
        assertEquals(2198155,player.hashCode())
    }

    void testEquals() {
        Player a = new Player("A")
        Player b = new Player( "b")
        ArrayList<Player> playerList = new ArrayList<Player>()
        playerList.add(a)
        playerList.add(b)
        playerList.add(player)
        assertThat(playerList.get(0),is(not(player)))
        assertThat(playerList.get(1),is(not(player)))
        assertThat(playerList.get(2),is(player))
    }

    void testCountHand7sand8s() {
        player.hand = handSevenAndEights
        // Double Pair Royale and 4 15-2s
        assertEquals( 12 + 8, player.countHand(handSevenAndEights, aceOfHearts))
    }

    void testCountHand16sand8s() {
        player.hand = handAceSixesAndEights
        // 2 pairs and 4 15-2s
        assertEquals( 4 + 8, player.countHand(handAceSixesAndEights,fiveOfHearts))
    }

    void testCountDoublePairRoyale() {
        // Hand
        player.hand = handSevenAndEights
        // Start Card
        player.hand.add(aceOfHearts)
        // 4 pairs - Double Royale
        assertEquals(12,player.countPairs(player.hand))
    }

    void testCountPairRoyale() {
        // A, 2, 8, 8, 8 will test for Pair Royale
        player.hand = handAceTwoEights
        // Start Card
        player.hand.add(sixOfHearts)
        // 3 pairs - Royale
        assertEquals(6,player.countPairs(player.hand))
    }

    void testCountPairs() {
        player.hand = handAceSixesAndEights
        // Start Card
        player.hand.add(fiveOfHearts)
        // 2 pairs - Pair(s)
        assertEquals(4,player.countPairs(player.hand))
    }

    void testCount15s() {
        player.hand = handSevenAndEights
        // Start Card
        player.hand.add(aceOfHearts)
        ArrayList<Card> combo = new ArrayList<Card>()
        ArrayList<Card> result = new ArrayList<Card>()
        assertEquals(8,player.count15s(player.getHand(),combo,2,result))
        assertEquals(0,combo.size())
        assertEquals(15,result.size())
    }

    void testCountRunsOfFive() {
        player.hand = handRunOf5AndFlush
        // Start Card
        player.hand.add(aceOfHearts)
        // run of 5
        assertEquals(5, player.countRuns(player.hand))
    }

    void testCountRunsOfFour() {
        player.hand = handFiveSixSevenAndEights
        // Start Card
        player.hand.add(aceOfHearts)
        // two runs of 4
        assertEquals(8, player.countRuns(player.hand))
    }

    void testCountRunsOfThree() {
        player.hand = handAceTwoThreeRuns
        // Start Card
        player.hand.add(fiveOfHearts)
        // two runs of 3
        assertEquals(6, player.countRuns(player.hand))
    }

    void testCountStraightWithHighFaceCards() {
        player.hand = handRoyalStraight
        player.hand.add(eightOfClubs)
        assertEquals(6,player.countRuns(player.hand))
    }

    void testCountTwoStraights() {
        player.hand = handTwoStraights
        // A,2,3 and J,O,K
        player.hand.add(kingOfClubs)
        assertEquals(6,player.countRuns(player.hand))
    }

    void testCountFlushWithoutStartCard() {
        player.hand = handRunOf5AndFlush
        // a five card run and a flush
        assertEquals(5,player.countFlush(player.hand,twoOfDiamonds))
    }

    void testCountFlushWithStartCard() {
        // 5, 6, 7, 8, 9
        player.hand = handRunOf5AndFlush
        player.hand.remove(4)
        // a five card run and a flush
        assertEquals(5,player.countFlush(player.hand,nineOfHearts))
    }

    void testCountNibsWithJack() {
        player.hand = handRoyalStraight
        // 8 - King of Clubs, Jack of clubs gives nibs with eight of clubs start card
        assertEquals(1,player.countOneForHisNobs(eightOfClubs))
    }

    void testCountNibsWithoutJack() {
        player.hand = handAceTwoThreeRuns
        // Ace, two, three runs with no Jack
        assertEquals(0,player.countOneForHisNobs(eightOfClubs))
    }

    void testCountNibsWithWrongJack() {
        handRoyalStraight.remove(jackOfClubs)
        handRoyalStraight.add(jackOfHearts)
        handRoyalStraight.remove(kingOfClubs)

        player.hand = handRoyalStraight
        // 8 - King of Clubs, Jack of clubs gives nibs with eight of clubs start card
        assertEquals(0,player.countOneForHisNobs(kingOfClubs))
    }

}
