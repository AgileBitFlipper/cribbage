package com.thirdsonsoftware

class GameTest extends GroovyTestCase {

    Game game

    ArrayList<Player> playerList


    private static final String NAME_PLAYER_A = "Player A"
    private static final String NAME_PLAYER_B = "Player B"

    private static final int INDEX_PLAYER_A = 0
    private static final int INDEX_PLAYER_B = 1

    private static final String NAME_PLAYER_FRED = "Fred"
    private static final String NAME_PLAYER_SALLY = "Sally"

    void setUp() {
        super.setUp()

        game = new Game(Game.DEFAULT_NUMBER_OF_PLAYERS)

        playerList = new ArrayList<Player>()
        playerList.add(new Player(NAME_PLAYER_FRED))
        playerList.add(new Player(NAME_PLAYER_SALLY))
    }

    void tearDown() {
    }

    void testValidatePlayerCountTooLow() {
        try {
            game.validatePlayerCount(Game.MINIMUM_NUMBER_OF_PLAYERS - 1)
            fail(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT)
        } catch (IllegalArgumentException iae) {
            assertEquals(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT, iae.getMessage())
        }
    }

    void testValidatePlayerCountTooHigh() {
        try {
            game.validatePlayerCount(Game.MAXIMUM_NUMBER_OF_PLAYERS + 1)
            fail(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT)
        } catch (IllegalArgumentException iae) {
            assertEquals(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT, iae.getMessage())
        }
    }

    void testPlay() {
    }

    void testSetupPlayersTooLow() {
        try {
            game.setupPlayers(Game.MINIMUM_NUMBER_OF_PLAYERS-1)
            fail(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT)
        } catch (IllegalArgumentException iae) {
            assertEquals(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT,iae.getMessage())
        }
    }

    void testSetupPlayersTooHigh() {
        try {
            game.setupPlayers(Game.MAXIMUM_NUMBER_OF_PLAYERS+1)
            fail(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT)
        } catch (IllegalArgumentException iae) {
            assertEquals(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT,iae.getMessage())
        }
    }

    void testSetupPlayers() {
        game.setupPlayers(Game.MINIMUM_NUMBER_OF_PLAYERS)
        assertEquals(Game.MINIMUM_NUMBER_OF_PLAYERS,game.getNumPlayers())
        assertEquals(Game.MINIMUM_NUMBER_OF_PLAYERS,game.getPlayers().size())
        assertEquals(NAME_PLAYER_A, game.getPlayer(INDEX_PLAYER_A).getName())
        assertEquals(NAME_PLAYER_B, game.getPlayer(INDEX_PLAYER_B).getName())
    }

    void testSetNumPlayers() {
        game.setupPlayers(Game.MINIMUM_NUMBER_OF_PLAYERS)
        assertEquals(Game.MINIMUM_NUMBER_OF_PLAYERS,game.getNumPlayers())
    }

    void testSetNumPlayersTooLow() {
        try {
            game.setupPlayers(Game.MINIMUM_NUMBER_OF_PLAYERS-1)
            fail(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT)
        } catch (IllegalArgumentException iae) {
            assertEquals(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT,iae.getMessage())
        }
    }

    void testSetNumPlayersTooHigh() {
        try {
            game.setupPlayers(Game.MAXIMUM_NUMBER_OF_PLAYERS+1)
            fail(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT)
        } catch (IllegalArgumentException iae) {
            assertEquals(Game.MESSAGE_CHOOSE_VALID_PLAYER_COUNT,iae.getMessage())
        }
    }

    void testGetNumPlayers() {
        assertEquals(Game.DEFAULT_NUMBER_OF_PLAYERS,game.getNumPlayers())
        game.setupPlayers(Game.MAXIMUM_NUMBER_OF_PLAYERS)
        assertEquals(Game.MAXIMUM_NUMBER_OF_PLAYERS,game.getNumPlayers())
    }

    void testGetPlayers() {
        assertEquals(Game.DEFAULT_NUMBER_OF_PLAYERS,game.getPlayers().size())
        assertEquals(NAME_PLAYER_A,game.getPlayers().get(INDEX_PLAYER_A).getName())
        assertEquals(NAME_PLAYER_B,game.getPlayers().get(INDEX_PLAYER_B).getName())
    }

    void testGetPlayer() {
        assertEquals(NAME_PLAYER_A,game.getPlayer(INDEX_PLAYER_A).getName())
        assertEquals(NAME_PLAYER_B,game.getPlayer(INDEX_PLAYER_B).getName())
    }

    void testSetPlayers() {
        assertEquals(NAME_PLAYER_A,game.getPlayer(INDEX_PLAYER_A).getName())
        game.setPlayers(playerList)
        assertEquals(NAME_PLAYER_SALLY,game.getPlayer(INDEX_PLAYER_B).getName())
    }

    void testToString() {
        assertEquals(
                "\n" +
                "\n" +
                "Game Results:\n" +
                "  Rounds:\n",game.toString())
    }

    void testGetRounds() {
        assertEquals("[]",game.getRounds().toString())
    }

    void testSetRounds() {
        game.setRounds( new ArrayList<Round>(
                Arrays.asList( new Round(0,playerList),
                        new Round(1,playerList))))
        assertEquals(2,game.getRounds().size())
    }

    void testDisplayRounds() {
        assertEquals("  Rounds:\n",game.displayRounds())
    }
}
