package com.thirdsonsoftware;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class CustomEventSerializer extends StdSerializer<Event> {

    public CustomEventSerializer() {
        this(null);
    }

    public CustomEventSerializer(Class<Event> t) {
        super(t);
    }

    @Override
    public void serialize(Event event, JsonGenerator jsonGenerator, SerializerProvider serializer) {
        try {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("eventDateTime", event.eventDateTime.toString());
            jsonGenerator.writeStringField("type", event.type.toString());
            jsonGenerator.writeFieldName("Player");
            jsonGenerator.writeObject(event.player);
            jsonGenerator.writeFieldName("Card");
            jsonGenerator.writeObject(event.card);
            jsonGenerator.writeNumberField("score", event.score);
            jsonGenerator.writeBooleanField("endOfRound", event.endOfRound);
            jsonGenerator.writeBooleanField("endOfGame", event.endOfGame);
            jsonGenerator.writeNumberField("round", event.round);
            jsonGenerator.writeNumberField("game", event.game);
            jsonGenerator.writeEndObject();
        } catch (IOException ioe) {
            Log.Error("  IOException serializing the Event object: " + ioe.getMessage());
        }
    }
}
