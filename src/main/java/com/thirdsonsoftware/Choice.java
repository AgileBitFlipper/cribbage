package com.thirdsonsoftware;

public class Choice {

    // Which card?
    Card card ;

    // If this choice is taken, what would the score be?
    private int score ;

    /**
     * Constructs a single choice based on a card, position, orientation,
     *   rotation and score.
     * Note:  Even though a Card object can hold row, col, orientation and
     *   rotation, it can't hold more than one set of these values.  That's
     *   why a choice was created.  The choice holds the card reference and
     *   how that card should be placed on the board for this particular
     *   instance.
     * @param card

     */
    public Choice(Card card) {
        this.card = card;
        this.score = 0 ;
    }

    /**
     * Get the Choice's card reference.
     * @return (Card)
     */
    public Card getCard() {
        return card;
    }

    /**
     * Set the card reference
     * @param card
     */
    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * Get the score for this Choice
     * @return
     */
    public int getScore() { return this.score ; }

    /**
     * Set the score for this Choice
     * @param s
     */
    public void setScore(int s) { this.score = s ; }

}
