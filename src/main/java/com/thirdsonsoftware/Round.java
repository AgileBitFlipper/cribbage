package com.thirdsonsoftware;

import java.io.PipedWriter;
import java.io.Serializable;
import java.util.*;
import java.util.stream.IntStream;

// todo: Move this to the card deck class jar for others to use
class CardOrdinalCompare implements Comparator<Card> {
    @Override
    public int compare(Card c1, Card c2) {
        return (Integer.valueOf(c1.getFace().ordinal())).compareTo(Integer.valueOf(c2.getFace().ordinal()));
    }
}

/**
 * The game of Triominos is composed of a series of Rounds.
 * Each Round ends when either a player reaches over 400 points,
 * or all possible plays for each player have been completed.
 * The game ends when a player reaches over 400 points.
 */
public class Round implements Serializable {

    // The roundNumber of this round
    private final int roundNumber;

    // The list of players playing the game
    private ArrayList<Player> players;

    // The card deck we are playing with
    private CardDeck deck;

    // The Crib hand
    private ArrayList<Card> crib;

    // The Start card (on top of deck)
    private Card cardStart ;

    // todo: The stack of played cards (maybe this should be per player?)
    private ArrayList<ArrayList<Card>> cardsAlreadyPlayed;

    // The stack of cards currently in play
    private ArrayList<Card> cardsInPlay ;

    private Player dealer ;

    /**
     * The default constructor for a Round.  Each Cribbage Game is a set of Rounds with alternating Players dealing
     *   for each Round played.
     * @param roundNumber which Round is this in the Game
     * @param players the list of Players in this Round
     */
    public Round(int roundNumber, ArrayList<Player> players) {

        // Validate the players
        if ( players == null )
            throw new IllegalArgumentException("A Round requires a list of players to be provided.");
        else if ( players.size() < 2 )
            throw new IllegalArgumentException("A Round requires that at least two players.");

        // Setup the roundNumber
        this.roundNumber = roundNumber;

        // Let's keep the list of players for each round
        this.players = players;

        Event.logEvent(EventType.GENERATE_CARDS,this);

        setDeck( new CardDeck() ) ;

        ArrayList<Card> cards ;

        cards = new ArrayList<Card>(CardDeck.MINIMUM_CUT);
        setCrib(cards);

        // Let's setup the list that will hold the cards from each round
        ArrayList<ArrayList<Card>> playedPerRound = new ArrayList<ArrayList<Card>>();
        cardsAlreadyPlayed = playedPerRound ;

        cards = new ArrayList<Card>(CardDeck.MINIMUM_CUT * getPlayers().size());
        setCardsInPlay(cards);

        for( Player p : this.players ) p.reset() ;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    /**
     * The main gameplay loop, looping through players and making plays based
     * on the rules of the game.
     */
    public Player playRound() {

        int indexPlayer;
        int turn = 1;

        Player player;
        Player lastPlayer=null;
        Player pWinner=null;

        Card cardPlayed;

        Log.Info("  Shuffling the deck...");
        Event.logEvent(EventType.SHUFFLE_CARDS);

        // Shuffle card pool
        getDeck().shuffleCards();

        // The player with the lowest value cutting the deck goes first
        Player dealer = cutForDealer();

        // Draw the initial cards for each player from the card pool
        dealCards();

        // Setup the starting player
        indexPlayer = (players.indexOf(dealer)+1) % getPlayers().size();
        player = players.get(indexPlayer);

        // Lay out for crib
        layOutForCrib() ;

        // Cut the deck
        cutDeckForStartCard() ;

        // Let's show the game board to everyone!
        Log.Info(this.toString());

        boolean aGo = false ;
        Player aGoPlayer = null ;

        Event.logEvent(EventType.PLAYER_STARTS,this, player);

        // TODO: Put a log Event for the fact that a new hand in the round starts here.
        Event.logEvent(EventType.HAND_STARTS, this);

        // While there are cards left to play, play them
        while (!allCardsPlayed()) {

            Log.Info(String.format(" Turn %d by %s ...", turn++, player.getName()));

            // Keep running through the players
            cardPlayed = player.playACard(getCardsInPlay(),this);

            // If the player played a card, let's update the necessary elements
            if (cardPlayed != null) {

                Event.logEvent(EventType.PLAY_A_CARD, this, player, cardPlayed);

                // Add the newly played piece to the list of pieces played
                getCardsInPlay().add(cardPlayed);

                Log.Info(String.format("  Player '%s' plays card '%s'.  Count on table is %d.",
                        player.getName(), cardPlayed,
                        getCardsInPlay().stream().mapToInt(c -> c.getValue()).sum()));

                // Do some magic for the player's hand so we don't lose the cards from the player's hand
                player.getHand().remove(cardPlayed);
                player.getPlayed().add(cardPlayed);

                // reset the blocked player count
                aGo = false;
                aGoPlayer = null;

                // The last player is me!
                lastPlayer = player ;

            } else {

                // Are we already in a 'go' situation?
                if ( aGo ) {

                    // Are we at the last player to 'go', the last player gets one for last card
                    if ( player != lastPlayer ) {

                        aGo = false;

                        player.setScore(player.getScore() + Game.POINTS_A_GO);
                        Log.Info(String.format(" Player '%s' scores one point for 'last card'...", player.getName()));
                        Event.logEvent(EventType.ONE_FOR_LAST_CARD,this, player);

                        // Now we need to reset the cards on table and start again
                        cardsAlreadyPlayed.add((ArrayList<Card>)getCardsInPlay().clone());
                        cardsInPlay.clear();

                        Log.Info(displayCards(true,"Flipping cards last play:", cardsAlreadyPlayed.get(cardsAlreadyPlayed.size()-1)));
                        players.stream().forEach(p -> Log.Info(displayCards(true, String.format("%s's remaining cards:",p.getName()), p.getHand())));

                        // TODO: Do we call this a HAND_OVER or something else.  We finished this hand....
                        Event.logEvent(EventType.HAND_OVER,this);

                        if (!allCardsPlayed()) {
                            Event.logEvent(EventType.HAND_STARTS,this);
                        }

                    } else {

                        Log.Info(String.format(" Player '%s' also signals a go...", player.getName()));
                        Event.logEvent(EventType.SIGNAL_A_GO,this, player);
                    }

                } else  {

                    // Does this player have an empty hand?  If so, let's log it.
                    if ( player.hasAnEmptyHand() ) {
                        Log.Info(String.format("  Player '%s' has no more cards.", player.getName()));
                    } else {
                        Log.Info(String.format("  Player '%s' has more cards to play.", player.getName()));
                    }

                    aGo = true ;
                    lastPlayer = player ;

                    Log.Info(String.format(" Player '%s' signals a go...", player.getName()));
                    Event.logEvent(EventType.SIGNAL_A_GO,this, player);
                }
            }

            // If all the cards have been played
            if ( allCardsPlayed() && ( cardsInPlay.size() == 0 ) ) {

                // Last card played scores a go
                player.setScore(player.getScore() + Game.POINTS_A_GO);
                Log.Info(String.format(" Player '%s' scores one point for 'last card'...", player.getName()));
                Event.logEvent(EventType.ONE_FOR_LAST_CARD,this, player);

                Log.Info(displayCards(true,"Flipping cards last play:", cardsAlreadyPlayed.get(cardsAlreadyPlayed.size()-1)));

                // TODO: Do we call this a HAND_OVER or something else.  We finished this hand....
                Event.logEvent(EventType.HAND_OVER,this);

            } else {

                // Choose the next player
                indexPlayer = (indexPlayer + 1) % players.size();
                player = players.get(indexPlayer);
            }
        }

        // Now we need to reset the cards on table and start again
        cardsAlreadyPlayed.add((ArrayList<Card>)getCardsInPlay().clone());
        Log.Info(displayCards(true,"Last hand played:", getCardsInPlay()));
        Event.logEvent(EventType.HAND_OVER,this);

        // The Round is over...let's account for points.
        Log.Info("\n\n ==== ROUND OVER === \n\n");

        // Todo: This shows the round details
        Log.Info(this.toString());

        Log.Info("\n\n ===== SHOW HANDS =====\n\n");

        // Let's score all the non-dealer hands
        pWinner = countHandsAndFindWinner();

        Log.Info("\n\n ===== END SHOW HANDS =====\n\n");
        return pWinner;
    }

    /**
     * Let's count all the non-dealer hands first.  That way, we can tell which non-dealer won by first count.
     */
    public Player countHandsAndFindWinner() {

        Player p = null ;

        // Find the dealer's index first
        int idxDealer = IntStream.range(0, getPlayers().size())
                .filter(indexPlayer-> getPlayers().get(indexPlayer).equals(getDealer()))
                .findFirst()
                .orElse(-1);

        if ( idxDealer == -1 )
            throw new IndexOutOfBoundsException("  Unable to find the dealer index.");

        // Walk through the player list from the dealer, round robin, until we visit all players
        for ( int idxPlayer = 0; idxPlayer < getPlayers().size(); idxPlayer++ ) {

            // Let's work with the index for the first player beyond the dealer
            int index = (idxPlayer+idxDealer+1)%getPlayers().size() ;

            // Get the player so we can score and determine if they are a winner
            p = getPlayers().get( index );

            // Set the crib card and count each player's hand
            int scoreHand = p.countHand(p.getPlayed(), getCardStart());
            p.setScore(p.getScore() + scoreHand);

            Event.logEvent(EventType.SHOW_HAND, this, p);

            Log.Info(String.format(" Player '%s' scores %d points with this hand for an overall score of %d.\n",
                    p.getName(), scoreHand, p.getScore()));

            // If we have a winner after the hand count, just declare it!
            if ( p.getScore() >= Game.POINTS_FOR_WIN ) {
                return p;
            }
        }

        // No winner yet...so now we need to score the crib and add it to the dealer's score
        int scoreCrib = dealer.countHand(getCrib(),getCardStart());
        dealer.setScore(dealer.getScore() + scoreCrib);
        Event.logEvent(EventType.SHOW_CRIB,this, dealer);
        Log.Info(String.format(" Dealer '%s' scores %d points with the crib for an overall score of %d.\n",
                dealer.getName(), scoreCrib, dealer.getScore()));

        p = players.stream()
                .peek(player -> Log.Info(String.format(" Will filter %s with score %d", player.getName(), player.getScore())))
                .filter(player -> !player.equals(getDealer()))
                .filter(player -> (player.getScore() >= Game.POINTS_FOR_WIN))
                .findFirst()
                .orElse(null);

        // Do we have a winner!
        if (p != null)
            Log.Info(String.format("Player '%s' has won the game by scoring %d points!  Congratulations!\n",
                    p.getName(), p.getScore()));

        return p;
    }

    public Player getDealer() {
        return dealer;
    }

    public void setDealer(Player dealer) {
        this.dealer = dealer;
    }

    private void cutDeckForStartCard() {

        Event.logEvent(EventType.CUT_DECK, this);

        Random randomSeed = new Random();

        // Look for the non-dealer to cut the deck
        Player nonDealer = players.stream().filter(p -> !p.getDealer()).findFirst().orElse(null);

        // Set the card start
        setCardStart(deck.getDeck().get(randomSeed.nextInt(getDeck().getDeck().size())));

        Event.logEvent(EventType.START_CARD, this, getCardStart());
        Log.Info(String.format("  Player '%s' cut deck.  Start card is '%s'.", nonDealer.getName(), getCardStart() ));

        // If the player cut a JACK, the dealer marks two points
        if ( getCardStart().getFace() == Face.JACK ) {

            Event.logEvent(EventType.TWO_FOR_HIS_HEELS, this);

            Log.Info(String.format("  Dealer '%s' marks one for 'His Heels' with start card '%s'.", getDealer().getName(), getCardStart()));

            // Mark two for His Heels
            getDealer().setScore(getDealer().getScore()+2);
        }
    }

    /**
     * Have each player lay out for Crib
     */
    private void layOutForCrib() {

        Event.logEvent(EventType.LAY_OUT_FOR_CRIB, this);

        Random randomSeed = new Random();

        int cardIndex = 0 ;
        int numberOfCardsToLayOut = getNumberOfCardsToLayOut() ;

        // For each player in the game
        for ( Player p : getPlayers() ) {

            // walk through the total number of times to pick a card for the crib
            for ( int countLayout = 0; countLayout < numberOfCardsToLayOut ; countLayout++ ) {

                // todo Randomly choose a card from our hand, later we will choose a hand based best match
                cardIndex = randomSeed.nextInt(p.getHand().size());

                Event.logEvent(EventType.LAY_OUT_FOR_CRIB, this, p, p.getHand().get(cardIndex));

                Log.Info(String.format("  Player %s lays out card '%s' (index %d) to crib.", p.getName(), p.getHand().get(cardIndex), cardIndex));

                // Lay it out to the crib
                getCrib().add(p.getHand().remove(cardIndex));
            }
        }

        Event.logEvent(EventType.CRIB,this);
    }

    private boolean allCardsPlayed() {
        boolean allHandsEmpty = true ;
        for ( Player p : getPlayers() ) {
            if ( p.getHand().size() != 0 ) {
                allHandsEmpty = false ;
                break;
            }
        }
        return allHandsEmpty ;
    }

    protected void setCrib(ArrayList<Card> cards) { this.crib = cards; }
    public ArrayList<Card> getCrib() { return crib; }

    /**
     * Sets the deck of cards in this Round that we are to play with
     * @param cards - the CardDeck to play with
     */
    protected void setDeck( CardDeck cards) {
        this.deck = cards;
    }

    /**
     * Returns the CardDeck in this Round that we are playing with
     * @return CardDeck for this Round
     */
    public CardDeck getDeck() {
        return deck;
    }

    protected void setCardsAlreadyPlayed(ArrayList<Card> cards) { this.cardsAlreadyPlayed.add(cards); }
    public ArrayList<Card> getCardsAlreadyPlayed(int indexRound) { return cardsAlreadyPlayed.get(indexRound); }

    protected void setCardsInPlay(ArrayList<Card> cards) { this.cardsInPlay = cards; }
    public ArrayList<Card> getCardsInPlay() { return cardsInPlay; }

    public ArrayList<Player> getPlayers() { return players; }

    /**
     * Spins through the players and performs a draw to pull the inital
     * number of cards that each player starts with
     */
    protected void dealCards() {

        Log.Info("  Dealing out the cards...");

        Event.logEvent(EventType.DEAL_HANDS, this);

        int numToPlayers = 0;
        int numToCrib = 0;

        numToPlayers = getNumberOfCardsToEachPlayer();
        numToCrib = getNumberOfCardsToDealCrib();

        Log.Info( String.format( " Drawing %d cards for each player's hand and %d to the crib...", numToPlayers, numToCrib ) ) ;

        while ( numToPlayers-- > 0 ) {

            for (Player p : getPlayers()) {
                Card c = getDeck().getDeck().remove(0);
                Log.Info(String.format("  Card %s is dealt to player '%s'", c, p.getName() ));
                p.getHand().add(c);
            }

            if ( numToCrib-- > 0 ) {
                Card c = getDeck().getDeck().remove(0);
                Log.Info(String.format("  Card %s is dealt to the crib", c ));
                crib.add(c);
            }
        }
    }

    protected void setCardStart(Card c) {
        Log.Info(String.format("  Start card set to %s",c));
        cardStart = c ;
    }

    protected Card getCardStart() {
        return cardStart ;
    }

    private int getNumberOfCardsToLayOut() {
        int numToLayOut = 0 ;
        switch (players.size()) {
            case 2:
                numToLayOut = 2 ;
                break;
            case 3:
                numToLayOut = 1 ;
                break;
            case 4:
                numToLayOut = 0 ;
                break;
            default:
                break;
        }
        return numToLayOut;
    }

    private int getNumberOfCardsToEachPlayer() {
        int numToPlayer = 0 ;
        switch (players.size()) {
            case 2:
                numToPlayer = 6 ;
                break;
            case 3:
                numToPlayer = 5 ;
                break;
            case 4:
                numToPlayer = 4 ;
                break;
            default:
                break;
        }
        return numToPlayer;
    }

    private int getNumberOfCardsToDealCrib() {
        int numToCrib = 0 ;
        switch (players.size()) {
            case 2:
                numToCrib = 0 ;
                break;
            case 3:
                numToCrib = 1 ;
                break;
            case 4:
                numToCrib = 0 ;
                break;
            default:
                break;
        }
        return numToCrib;
    }

    /**
     * Scans through the list of players, allowing each
     *   player to cut the deck.  The lowest card wins
     *   the deal.  You need to have at least 4 cards
     *   in the cut, and there have to be enough cards
     *   left for the others to cut with.
     *
     * @return the player that starts the game
     */
    protected Player cutForDealer() {

        Log.Info("  Cutting for dealer...");

        Event.logEvent(EventType.CUT_FOR_LOWEST_CARD, this);

        int cardIndex = 0 ;
        Card lowestCard = null;

        // This call cuts the deck for each player following the cut rules
        List<Card> playerCuts = getDeck().cutDeck(getPlayers().size());

        // Get the index of the player with the lowest ordinal
        int idxPlayer = playerCuts.indexOf(Collections.min(playerCuts, new CardOrdinalCompare()));

        lowestCard = playerCuts.get(idxPlayer);
        setDealer(players.get(idxPlayer));
        getDealer().setDealer(true);

        Event.logEvent(EventType.PLAYER_DEALS,this, getDealer());

        Log.Info(String.format("  Player '%s' will deal.  Lowest card was '%s'.", dealer.getName(), lowestCard));
        return getDealer();
    }

    protected String displayCards(Boolean asCard, String name, ArrayList<Card> list) {
        StringBuilder strCards = new StringBuilder();
        if ( list == null ) {
            strCards.append("<null>");
        } else {
            strCards.append(String.format("%s (%d):\n", name, list.size()));
            if (list.isEmpty()) {
                strCards.append("  [<empty>]\n");
            } else {
                if (asCard) {
                    String rows[] = new String[5];
                    int r;
                    for (int i = 0; i < 5; i++)
                        rows[i] = "  ";
                    for (Card card : list) {
                        getDeck().displayCard(CardDeck.DISPLAY_AS_A_CARD, card, rows);
                    }
                    for (String str : rows)
                        strCards.append(str + "\n");
                } else {
                    strCards.append("  [");
                    for (Card card : list) {
                        if (list.lastIndexOf(card) != list.size() - 1)
                            strCards.append(card).append(", ");
                        else
                            strCards.append(card);
                    }
                    strCards.append("]\n");
                }
            }
        }
        return strCards.toString();
    }

    protected String displayDeck() {
        return displayCards(true, " Card Deck", new ArrayList<Card>(getDeck().getDeck()));
    }

    protected String displayCrib() {
        return displayCards(true, " Crib", getCrib());
    }

    protected String displayCardsInPlay() {
        return displayCards(true, " Cards In Play", getCardsInPlay());
    }

    protected String displayCardsAlreadyPlayed() {
        StringBuilder strBuilder = new StringBuilder();
        int index=1;
        for ( ArrayList<Card> cards : cardsAlreadyPlayed )
            strBuilder.append( displayCards(true, String.format(" Played Cards Hand %d:", index++), cards));
        return strBuilder.toString();
    }

    protected String displayPlayers() {
        StringBuilder playersString = new StringBuilder(200);
        playersString.append(" Players (").append(getPlayers().size()).append("):\n");
        for (Player p : getPlayers()) {
            playersString.append(p);
        }
        return playersString.toString();
    }

    protected String displayStartCard() {
        StringBuilder strStart = new StringBuilder(100);
        strStart.append(displayCards(true," Start card",
                new ArrayList<Card>(Arrays.asList(cardStart))));
        return strStart.toString();
    }

    public String toString() {
        return ("Round " + this.roundNumber + ":\n" +
                displayPlayers() +
                //displayDeck() +
                displayStartCard() +
                displayCrib() +
                displayCardsAlreadyPlayed());
    }

}
