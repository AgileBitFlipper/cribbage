package com.thirdsonsoftware;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

enum EventType {
    START_A_GAME,
    START_A_ROUND,
    SETUP_PLAYERS,
    GENERATE_CARDS,
    SHUFFLE_CARDS,
    CUT_FOR_LOWEST_CARD,
    PLAYER_DEALS,
    PLAYER_STARTS,
    CUT_DECK,
    DEAL_HANDS,
    LAY_OUT_FOR_CRIB,
    CRIB,
    START_CARD,
    TWO_FOR_HIS_HEELS,
    HAND_STARTS,
    PLAY_A_CARD,
    SIGNAL_A_GO,
    FIFTEEN_TWO_DURING_GAMEPLAY,
    THIRTY_ONE_DURING_GAMEPLAY,
    ONE_FOR_LAST_CARD,
    PAIR_GAMEPLAY,
    PAIR_ROYALE_GAMEPLAY,
    DOUBLE_PAIR_ROYALE_GAMEPLAY,
    THREE_CARD_SEQUENCE_GAMEPLAY,
    FOUR_CARD_SEQUENCE_GAMEPLAY,
    FIVE_CARD_SEQUENCE_GAMEPLAY,
    SIX_CARD_SEQUENCE_GAMEPLAY,
    SEVEN_CARD_SEQUENCE_GAMEPLAY,
    EIGHT_CARD_SEQUENCE_GAMEPLAY,
    HAND_OVER,
    SHOW_HAND,
    SHOW_CRIB,
    FIFTEEN_TWO,
    PAIR,
    PAIR_ROYALE,
    DOUBLE_PAIR_ROYALE,
    THREE_CARD_SEQUENCE,
    FOUR_CARD_SEQUENCE,
    FIVE_CARD_SEQUENCE,
    SIX_CARD_SEQUENCE,
    SEVEN_CARD_SEQUENCE,
    EIGHT_CARD_SEQUENCE,
    FOUR_CARD_FLUSH,
    FIVE_CARD_FLUSH,
    HIS_NOBS,
    END_A_ROUND,
    WIN_A_GAME,
    END_A_GAME,
}

public class Event implements Serializable {

    // When did the event occur
    private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    Date eventDateTime;

    // What type of event was this
    EventType type;

    // Played by
    Player player;

    // Card played
    Card card;

    // Each game consists of multiple rounds
    int round;

    // Score received for play
    int score;

    // Final round
    boolean endOfGame;

    // End of a Round
    boolean endOfRound;

    // Making the game static allows us to set it and forget it
    // Todo: Might want to consider this for other options?  Round?
    public static int game = 1;

    // Generic Event
    public Event(EventType evtType) {
        type = evtType;
        eventDateTime = new Date();
    }

    // Default constructor
    public Event(EventType evtType, Player p, Card c, Round r) {
        eventDateTime = new Date();
        type = evtType;
        round = r.getRoundNumber();
        player = p;
        card = c;
    }

    public String toString() {
        StringBuilder strEvent = new StringBuilder(100);
        strEvent.append(eventDateTime).append(",");
        strEvent.append("Type:").append(type).append(",");
        strEvent.append("Game:").append(game).append(",");
        strEvent.append("Round:").append(round).append(",");
        strEvent.append("Name:").append((player != null) ? player.getName() : "").append(",");
        strEvent.append("Card:").append((card != null) ? card : "").append(",");
        strEvent.append("Score:").append(score);
        return strEvent.toString();
    }

    static void logEvent(Event evt) {
        EventManager.getInstance().logEvent(evt);
    }

    static void logEvent(EventType type) {
        Event event = new Event(type);
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, int value) {
        Event event = new Event(type);
        if (type == EventType.START_A_GAME || type == EventType.END_A_GAME) {
            event.game = value;
        } else if (type == EventType.START_A_ROUND || type == EventType.END_A_ROUND) {
            event.round = value;
        }
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Player p) {
        Event event = new Event(type);
        event.player = p ;
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Round r) {
        Event event = new Event(type);
        event.round = r.getRoundNumber();
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Round r, Card c) {
        Event event = new Event(type);
        event.round = r.getRoundNumber();
        event.card = c;
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Round r, Player p) {
        Event event = new Event(type);
        event.round = r.getRoundNumber();
        event.player = p;
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Round r, Player p, Card c) {
        Event event = new Event(type);
        event.card = c;
        event.player = p;
        event.round = r.getRoundNumber();
        EventManager.getInstance().logEvent(event);
    }

}
