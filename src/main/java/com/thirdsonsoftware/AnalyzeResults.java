package com.thirdsonsoftware;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Comparator;

/**
 * This is the class designed to brute force analyze the raw data for
 *   each game.
 *
 *   Futures:  
 *   This action should be replaced by the use of the ELK
 *   stack.  Logstash would read the JSON file data and send it to
 *   Elasticsearch.  Then Kibana would digest that data and present it
 *   in a format that the user desires.
 *   Still further, this action could be used to help analyze and 
 *   direct gameplay.  With directed gameplay, decisions about each move
 *   would be influenced by the data collected from previous gameplay,
 *   thus providing a improved experience and score.
 */
public class AnalyzeResults {

    /**
     * The ValueComparator class is a comparator using a generic type.  This provides
     *   a quick way to sort Key-Value pairs by Value without having to rewrite it
     *   for each Key type.
     * @param <K> the key type
     * @param <V> the value type
     */
    class ValueComparator<K, V extends Comparable<V>> implements Comparator<K>{

        HashMap<K, V> map = new HashMap<K, V>();

        public ValueComparator(HashMap<K, V> map){
            this.map.putAll(map);
        }

        @Override
        public int compare(K s1, K s2) {
            V value1 = map.get(s1);
            V value2 = map.get(s2);
            // The order of the elements, ascending or descending, is determined by
            //   the sign given to the compare result.  If '-', the largest value is
            //   first.  If '+', the smallest value is first.  If '0', you can decide
            //   how to order the elements.
            int order = -value1.compareTo(value2);
            if ( order == 0 ) order = -1 ;
            return order ;
        }
    }

    Date dateOfAnalysis ;

    // Simple counts
    int numberOfEventFiles          = 0 ;
    int cardsPlayedInRound          = 0 ;
    int cardsPlayedInGame           = 0 ;
    int numberOfRoundsPlayed        = 0 ;
    int numberOfHandsPlayed         = 0 ;
    int numberOfGamesPlayed         = 0 ;
    int numberOfPairsDuringGameplay = 0 ;

    // Counts with objects
    HashMap<Player, Integer> playersThatWonAGame = new HashMap<Player, Integer>();
    HashMap<Player, Integer> playersThatWonARound = new HashMap<Player, Integer>();
    HashMap<Card, Integer> cardsPlayed = new HashMap<Card, Integer>();
    HashMap<Card, Integer> startingCardsPlayed = new HashMap<Card, Integer>();

    // Lists
    List<Integer> listCardsPlayedInRound = new ArrayList<Integer>();
    List<Integer> listCardsPlayedInGame = new ArrayList<Integer>();

    public AnalyzeResults() {

        // Let's go analyze the data..
        analyze();
    }

    public void showResultsOfAnalysis() {

        Log.Info(showDateTimeOfAnalysis());
        Log.Info(String.format("  %d Event files analyzed.",numberOfEventFiles));

        Log.Info(showNumberOfGames());
        Log.Info(showNumberOfRounds());
        Log.Info(showNumberOfHands());
        Log.Info(showNumberOfPairsDuringGameplay());
        Log.Info(showAverageNumberOfCardsInARound());
        Log.Info(showAverageNumberOfCardsInAGame());
        Log.Info(showGamesWonByAPlayer());
        Log.Info(showRoundsWonByAPlayer());
        Log.Info(showCardsPlayed());
        Log.Info(showStartingCardPlayed());
    }

    /**
     * It's important to know when you ran this analysis.  Maybe it's a bit stale.
     * @return date and time in string form "yyy/MM/dd HH:mm:ss"
     */
    private String showDateTimeOfAnalysis() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return (String.format("\n Analysis of Events was performed @ %s",
                dateFormat.format(dateOfAnalysis)));
    }

    /**
     * Simply count up the number of times that the START_GAME event appears in the event logs.
     * @return the "number of games" header with count in String form
     */
    private String showNumberOfGames() {
        return String.format("  Number of games played: %d", numberOfGamesPlayed);
    }

    /**
     * Simply count up the number of times that the START_ROUND event appears in the event logs.
     * @return the "number of rounds" header with count in String form
     */
    private String showNumberOfRounds() {
        return String.format("  Number of rounds played: %d", numberOfRoundsPlayed);
    }

    /**
     * Simply count up the number of times that the START_HAND event appears in the event logs.
     * @return the "number of hands" header with count in String form
     */
    private String showNumberOfHands() {
        return String.format("  Number of hands played: %d", numberOfHandsPlayed);
    }

    /**
     * Simply count up the number of times that a PAIR_GAMEPLAY event appears in the event logs.
     * NOTE:  A PAIR_GAMEPLAY event occurs during gameplay while a PAIR event happens during
     *        the show.
     * @return the "number of pairs" header with count in String form
     */
    private String showNumberOfPairsDuringGameplay() {
        return String.format("  Number of pairs during gameplay: %d", numberOfPairsDuringGameplay);
    }

    /**
     * Add up all of the cards played per round and divide them by the number of rounds played.
     * @return the "average cards in a round" header with count in String form
     * todo: Show how this could be accomplished with a lambda expression
     */
    private String showAverageNumberOfCardsInARound() {
        int sum = 0;
        Iterator<Integer> iterator = listCardsPlayedInRound.iterator();
        while( iterator.hasNext() ) {
            sum += iterator.next().intValue();
        }
        int denominator = listCardsPlayedInRound.size() ;
        int average = ( denominator == 0) ? 0 : (sum/denominator) ;
        return String.format("  Average number of Cards played in a Round: %d", average);
    }

    /**
     * Add up all of the cards played per game and divide them by the number of games played.
     * @return the "average cards in a game" header with count in String form
     */
    private String showAverageNumberOfCardsInAGame() {
        int sum = 0;
        Iterator<Integer> iterator = listCardsPlayedInGame.iterator();
        while( iterator.hasNext() ) {
            sum += iterator.next().intValue();
        }
        int denominator = listCardsPlayedInGame.size() ;
        int average = ( denominator == 0) ? 0 : (sum/denominator);
        return String.format("  Average number of Cards played in a Game: %d", average);
    }

    /**
     * Determines the winner of each game, building a sorted TreeMap of Player and wins.  The
     *   results of this are put into String form and returned.
     * @return the complete list of Players and how many times they have won in String form.
     */
    public String showGamesWonByAPlayer() {

        Comparator<Player> playerComparator = new ValueComparator<Player, Integer>(playersThatWonAGame);
        TreeMap<Player, Integer> sortedPlayers = new TreeMap<Player, Integer>(playerComparator);
        sortedPlayers.putAll(playersThatWonAGame);

        StringBuilder strGamesWonByPlayer = new StringBuilder(100).append("  Games won by players:");

        Set set = sortedPlayers.entrySet() ;
        Iterator it = set.iterator();
        while ( it.hasNext() ) {
            Map.Entry me = (Map.Entry)it.next();
            Player player = (Player)me.getKey();
            strGamesWonByPlayer.append(String.format("\n   Player '%s' has won %d times.", player.getName(), me.getValue()));
        }
        return strGamesWonByPlayer.toString();
    }

    /**
     * Determines the winner of each round, building a sorted TreeMap of Player and wins.  The
     *   results of this are put into String form and returned.
     * @return the complete list of Players and how many times they have won in String form.
     */
    public String showRoundsWonByAPlayer() {

        Comparator<Player> playerComparator = new ValueComparator<Player, Integer>(playersThatWonARound);
        TreeMap<Player, Integer> sortedPlayers = new TreeMap<Player, Integer>(playerComparator);
        sortedPlayers.putAll(playersThatWonARound);

        StringBuilder strRoundsWonByPlayer = new StringBuilder(100).append("  Rounds won by players:");

        Set set = sortedPlayers.entrySet() ;
        Iterator it = set.iterator();
        while ( it.hasNext() ) {
            Map.Entry me = (Map.Entry)it.next();
            Player player = (Player)me.getKey();
            strRoundsWonByPlayer.append(String.format("\n    Player '%s' has won %d times.", player.getName(), me.getValue()));
        }
        return strRoundsWonByPlayer.toString();
    }

    /**
     * Builds a sorted TreeMap of Cards and the count of how many times that card has been played.
     *   The results of this are put into String form and returned.
     * @return the complete list of Cards and how many times each has been played in String form.
     */
    public String showCardsPlayed() {
        Comparator<Card> cardComparator = new ValueComparator<Card, Integer>(cardsPlayed);
        TreeMap<Card,Integer> sortedCards = new TreeMap<Card, Integer>(cardComparator);
        sortedCards.putAll(cardsPlayed);

        StringBuilder strCardsPlayed = new StringBuilder(100).append("  Cards played:");

        Set set = sortedCards.entrySet();
        Iterator it = set.iterator();
        while ( it.hasNext() ) {
            Map.Entry me = (Map.Entry)it.next();
            Card card = (Card)me.getKey();
            strCardsPlayed.append(String.format("\n    Card '%s' (%2d) has been played %s times.", me.getKey(), card.getId(), me.getValue()));
        }
        return strCardsPlayed.toString();
    }

    /**
     * Builds a sorted TreeMap of Cards and how many times that card has been the Start Card.  The
     *   results of this are put into String form and returned.
     * @return the complete list of Cards and how many times they have been the Start Card in
     *   String form.
     */
    public String showStartingCardPlayed() {
        Comparator<Card> cardComparator = new ValueComparator<Card, Integer>(startingCardsPlayed);
        TreeMap<Card,Integer> sortedCards = new TreeMap<Card, Integer>(cardComparator);
        sortedCards.putAll(startingCardsPlayed);

        StringBuilder strCardsPlayed = new StringBuilder(100).append("  Cards played at start:");

        Set set = sortedCards.entrySet();
        Iterator it = set.iterator();
        while ( it.hasNext() ) {
            Map.Entry me = (Map.Entry)it.next();
            Card card = (Card)me.getKey();
            strCardsPlayed.append(String.format("\n    Card '%s' (%2d) has been played as the starting card %s times", me.getKey(), card.getId(), me.getValue()));
        }
        return strCardsPlayed.toString();
    }

    /**
     * How many times has each Player won a game?
     * How many times has a Pair been scored during gameplay?
     * How many times has a Card been played?
     * How many times has a Card been used to start a Round?
     * What is the average number of cards played in a Round?
     * What is the highest number of cards played in a Game?
     */
    public void analyze() {

        // Always set the date-time of the analysis
        dateOfAnalysis =  new Date() ;

        // Event files
        List<String> eventFiles = EventManager.getInstance().getAllEventDataFiles();
        numberOfEventFiles = eventFiles.size() ;
        for (String eventFile : eventFiles) {

            // Start with no cards played/counted
            cardsPlayedInGame = 0 ;
            cardsPlayedInRound = 0 ;

            // Events in file
            List<Event> events = EventManager.getInstance().getAllEventsForDataFile(eventFile);
            for (Event event : events) {

                switch ( event.type ) {

                    case WIN_A_GAME: {
                        Player player = event.player;
                        if (playersThatWonAGame.containsKey(player)) {
                            playersThatWonAGame.put(player, playersThatWonAGame.get(player) + 1);
                        } else {
                            playersThatWonAGame.put(event.player, 1);
                        }
                        listCardsPlayedInGame.add(cardsPlayedInGame);
                    }
                    break;

                    case END_A_ROUND:
                    {
                        Player player = event.player;
                        listCardsPlayedInRound.add(cardsPlayedInRound);
                        cardsPlayedInRound = 0 ;
                    }
                    break;

                    case PLAY_A_CARD:
                    {
                        cardsPlayedInRound++;
                        cardsPlayedInGame++;

                        Card c = event.card;
                        if (cardsPlayed.containsKey(c)) {
                            cardsPlayed.put(c, cardsPlayed.get(c) + 1);
                        } else {
                            cardsPlayed.put(c, 1);
                        }
                    }
                    break;

                    case START_CARD: {
                        Card c = event.card;
                        if(startingCardsPlayed.containsKey(c)) {
                            startingCardsPlayed.put(c, startingCardsPlayed.get(c) + 1 ) ;
                        } else {
                            startingCardsPlayed.put(c,1);
                        }
                    }
                    break;

                    case PAIR_GAMEPLAY:
                        numberOfPairsDuringGameplay++;
                        break;

                    case HAND_STARTS:
                        numberOfHandsPlayed++;
                        break;

                    case START_A_GAME:
                        numberOfGamesPlayed++;
                        break;

                    case START_A_ROUND:
                        numberOfRoundsPlayed++;
                        break;
                }
            }
        }
    }

}
