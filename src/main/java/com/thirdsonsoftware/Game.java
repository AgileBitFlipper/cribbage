/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("SpellCheckingInspection")
class Game implements Serializable {

    /**
     * How many players will be in the game if none specified
     */
    protected static final int DEFAULT_NUMBER_OF_PLAYERS = 2;

    /**
     * What is the minimum number of players allowed?
     */
    protected static final int MINIMUM_NUMBER_OF_PLAYERS = 2;

    /**
     * What is the maximum number of players allowed?
     */
    protected static final int MAXIMUM_NUMBER_OF_PLAYERS = 6;

    /**
     * Messages used and should be translated
     */
    protected static final String MESSAGE_CHOOSE_VALID_PLAYER_COUNT = "Cribbage is designed for 2 to 6 players." ;

    /**
     * How many cards per hand if we are playing six-card cribbage?
     *   Six are dealt out to each player.  Each player keeps four.
     *   Two from each hand are put in the crib, making each player
     *   and the crip wind up with four cards.
     */
    public static final int CARDS_IN_HAND = 4 ;

    /**
     * Define how many points a player can earn during the game
     */
    public static final int POINTS_FOR_WIN              = 61;

    public static final int POINTS_A_GO                 = 1 ;
    public static final int POINTS_A_PAIR               = 2 ;
    public static final int POINTS_A_PAIR_ROYAL         = 6 ;
    public static final int POINTS_A_DOUBLE_PAIR_ROYAL  = 12 ;

    public static final int POINTS_15_2                 = 2 ;
    public static final int POINTS_31                   = 2 ;

    public static final int MAX_POINTS_PER_ROUND        = 31 ;

    /**
     * How many players are playing this game?
     */
    private int numPlayers;

    /**
     * This is the List of players in the game
     */
    private ArrayList<Player> players;

    /**
     * Each game is composed of a set of Rounds with each round marking the
     *  change of dealer.
     */
    private ArrayList<Round> rounds;

    /**
     * Default constructor for a Game.
     * @param numPlayers (int) number of games we want to play
     */
    Game(int numPlayers) {

        validatePlayerCount(numPlayers);

        Log.Info(String.format("Let's play %d-player cribbage!", getNumPlayers()));

        setupPlayers(getNumPlayers());

        // Allocate at least two rounds for each game...more added as needed
        setRounds(new ArrayList<Round>(2));
    }

    /**
     * Let's play the game of Triominos!!
     */
    protected void play() {

        // We're tracking each time we play a round
        int numRound = 0;

        // Be prepared to announce the winner of the game.
        final AtomicReference<Player> playerWonGame = new AtomicReference<Player>();

        // We need to kick off the rounds.
        do {
            ++numRound;

            Log.Info(String.format("  ======== Starting Round %d ========", numRound));

            // Setup a new Round with the list of players
            Round round = new Round( numRound, getPlayers() );
            getRounds().add(round);

            Event.logEvent(EventType.START_A_ROUND, round);

            // Let's play a round
            playerWonGame.set(round.playRound());

            Event.logEvent(EventType.END_A_ROUND, round);

            Log.Info(String.format("\n\n  ======== End Of Round %d ========\n\n", numRound));

        } while (playerWonGame.get() == null);

        Event.logEvent(EventType.WIN_A_GAME, playerWonGame.get());

        Log.Info("  ======== Game Results ========\n");
        Log.Info(String.format("  Game completed in %d rounds.", numRound));
        Log.Info(String.format("  Player '%s' won the game with %d points.",
                playerWonGame.get().getName(), playerWonGame.get().getScore()));
        getPlayers().stream().filter(p -> !p.equals(playerWonGame.get())).forEach(p -> Log.Info(
                String.format("  Player '%s' score was %d points.", p.getName(), p.getScore())));
        Log.Info("\n  ======== Game Results ========\n");
    }

    /**
     * Let's create the players for the game
     * @param playerCount the number of players in the game
     */
    protected void setupPlayers(int playerCount ) {

        // Only allow setup for the proper number of players, throw if invalid
        if ( validatePlayerCount(playerCount)) {

            Event.logEvent(EventType.SETUP_PLAYERS);

            // Allocate the number of players specified
            setPlayers(new ArrayList<Player>(playerCount));

            // Assign player names
            for (int i = 0; i < playerCount; i++) {
                getPlayers().add(new Player(String.format("Player %c", 'A' + i)));
            }
        }
    }

    /**
     * There is a minimum and maximum number of players allowed.  Validate and
     *    setup for the number of players specified.
     * @param numPlayers how many players want to play the game
     * @return true if a valid number of players.
     */
    protected boolean validatePlayerCount(int numPlayers) {

        if ( numPlayers < MINIMUM_NUMBER_OF_PLAYERS || numPlayers > MAXIMUM_NUMBER_OF_PLAYERS ) {
            throw new IllegalArgumentException(
                    String.format("Cribbage is designed for %d to %d players.",
                            MINIMUM_NUMBER_OF_PLAYERS, MAXIMUM_NUMBER_OF_PLAYERS));
        }

        setNumPlayers(numPlayers);
        Log.Info(String.format(" Setting up for %d players.", numPlayers));

        return true;
    }

    /**
     * The number of players needs to be setup so we can deterine
     * the number of pieces per player, and how each draw is
     * handled.
     *
     * @param numPlayers the number of Players in the game
     */
    protected void setNumPlayers(int numPlayers) {

        // Set the number of players first
        if ((numPlayers >= MINIMUM_NUMBER_OF_PLAYERS) && (numPlayers <= MAXIMUM_NUMBER_OF_PLAYERS)) {
            this.numPlayers = numPlayers;
        } else {
            this.numPlayers = DEFAULT_NUMBER_OF_PLAYERS;
        }
    }

    /**
     * How many players are in this game?
     *
     * @return (int) number of Players in the game
     */
    protected int getNumPlayers() {
        return this.numPlayers;
    }

    /**
     * The list of players in the game.
     *
     * @return players List of Players in the game
     */
    protected ArrayList<Player> getPlayers() {
        return players;
    }

    /**
     * Returns back a single player from the list of players in the game
     *
     * @param index - the index of which player is requested
     * @return (Player) the instance of the player that is specified.
     */
    protected Player getPlayer(int index) {

        // Validate the index
        if ( ( index < 0 ) || ( index >= getPlayers().size() ) ) {
            throw new InvalidParameterException(String.format("Index %d out of bounds for Player list!", index));
        } else {
            return getPlayers().get(index);
        }
    }

    /**
     * Establishes the list of players in the Game.
     *
     * @param players (ArrayList<Player>) - Array list of players in the game.
     */
    protected void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    /**
     * Provides a text view of the current game
     * the player list, and the current board.
     *
     * Note:  Must be public to override
     *
     * @return String containing a snapshot view of the current Game.
     */
    public String toString() {
        return ( "======== Game Results - Round Details ========\n\n" +
                 "Rounds:\n" +
                 displayRounds() +
                 "\n======== Game Results - Round Details ========\n" );
    }

    /**
     * Games are played in Rounds until a player scores above 400 points.
     *
     * @return the List of Rounds played
     */
    protected List<Round> getRounds() {
        return rounds;
    }

    /**
     * Set the list of rounds
     *
     * @param rounds the List of Rounds for the Game
     */
    protected void setRounds(ArrayList<Round> rounds) {
        this.rounds = rounds;
    }

    /**
     * We need a way to display the final stats based on the Rounds played
     *
     * @return (String) a string representing the Rounds played for this Game
     */
    protected String displayRounds() {
        StringBuilder strRounds = new StringBuilder(500);
        for (Round r : getRounds()) {
            strRounds.append(r.toString());
        }
        return ("  Rounds:\n" + strRounds);
    }
}
