/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import java.io.Serializable;
import java.util.*;

/**
 * A player for the game of triominos
 */
public class Player implements Serializable {

    private int wonAGameCount;       // How many times has this player won a game
    private int score;               // The players score
    private boolean starts;          // This player starts the game (highest card)
    private String name;             // The name of the player

    private ArrayList<Card> hand;    // The hand in the player's hand
    private ArrayList<Card> played;  // The cards that have been played, but are no longer in play this round.

    //    private ArrayList<Card> inPlay;  // The cards that are currently inPlay.

    /**
     * Let's construct a new player with the given name
     * @param name - name of player
     */
    public Player(String name) {
        setWonAGameCount(0);
        setScore(0);
        setDealer(false);
        setName(name);
        hand = new ArrayList<>(CardDeck.DEFAULT_COUNT);
        played = new ArrayList<>(CardDeck.DEFAULT_COUNT);
        setDealer(false);
    }

    /**
     * Reset the values in a player that should be reset between Rounds
     */
    public void reset() {
        starts = false ;
        hand.clear();
        played.clear();
    }

    /**
     * How many games has this player won?
     *
     * @param count
     */
    public void setWonAGameCount(int count) {
        wonAGameCount = count;
    }

    public int getWonAGameCount() {
        return wonAGameCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) { this.score = score; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getDealer() { return starts; }

    public void setDealer(boolean starts) { this.starts = starts; }

    public ArrayList<Card> getHand() { return hand; }

    public void setHand( ArrayList<Card> hand ) { this.hand = hand ; }

    public ArrayList<Card> getPlayed() { return played; }

    protected boolean hasWon() { return (getScore() >= Game.POINTS_FOR_WIN); }

    protected boolean hasAnEmptyHand() {
        return (getHand().size() == 0);
    }

    /**
     * Let this Player play a card.
     * @param onTable
     * @param round
     * @return
     */
    public Card playACard(ArrayList<Card> onTable, Round round ) {

        //Log.Info(displayCards(true,"Cards this round:", onTable));

        // Tell me what the table totals to right now
        int tableSum = onTable.stream().mapToInt(c -> c.getValue()).sum();

        // Let's see if we have any choices to play
        ArrayList<Choice> choicesToPlay = new ArrayList<Choice>();

        // Let's look at our played cards and see if any have available faces...
        //   Add all choices to the list so we can find the most valuable
        Iterator<Card> iCard = getHand().iterator();
        while (iCard.hasNext()) {
            Card played = iCard.next();
            if ( tableSum + played.getValue() < Game.MAX_POINTS_PER_ROUND ) {
                generateChoice(choicesToPlay,onTable,played);
                Log.Info(String.format("   Playing card '%s' yields %d points...",
                        played, choicesToPlay.get(choicesToPlay.size()-1).getScore()));
            } else {
                Log.Debug(String.format("   Playing card '%s' would exceed maximum points per round (%d).",
                        played, Game.MAX_POINTS_PER_ROUND ));
            }
        }

        Card cardToPlay = null;

        // Only attempt to work with a choice if there is one.
        if ( choicesToPlay.size() > 0 ) {

            // Of all the choices, pick the one with the maximum benefit in score.
            Choice topChoice = Collections.max(choicesToPlay, Comparator.comparing(c -> c.getScore()));

            // Do we have a top choice from the list of choices?
            if (topChoice != null) {

                cardToPlay = topChoice.getCard();

                Log.Debug(String.format("  Score %d with top choice card %s", topChoice.getScore(), topChoice.getCard()));

                Event.logEvent(EventType.PLAY_A_CARD, round, topChoice.getCard());

                this.setScore(this.getScore() + topChoice.getScore());

            } else {

                // If we can't find a top choice, choose the first in the list
                topChoice = choicesToPlay.get(0);
                cardToPlay = topChoice.getCard();
            }
        } else {
            Log.Info(String.format("  Player '%s' calls a 'go'.", this.getName()));
        }

        return cardToPlay;
    }

    /**
     * Given the card on the table, generate all possible choices for the next card being played.
     * @param choicesToPlay the list of choices to play
     * @param onTable the list of cards that have been played
     * @param played
     */
    public void generateChoice(ArrayList<Choice> choicesToPlay, ArrayList<Card> onTable, Card played) {

        int points = 0 ;

        Choice choice = new Choice(played) ;

        points += count15sAnd31sOnTable(onTable,played);
        points += countPairsOnTable(onTable,played);
        points += countRunsOnTable(onTable,played);

        choice.setScore(points);
        choicesToPlay.add(choice);
    }

    /**
     * This method will be used to count the possible run currently on the
     *   table.  Double runs are NOT counted, only single continuous sequences
     *   of cards.
     *
     *   Sample Gameplay
     *   ===============
     *   Player 1: A     1   -   0
     *   Player 2: 3     4   -   0
     *   Player 1: 4     8   -   0
     *   Player 2: 2     10  -   4
     *   Player 1: 5     15  -   2 + 5
     *   Player 2: 2     17  -   0
     *   Player 1: 4     21  -   0
     *   Player 2: 3     24  -   4
     *   Player 1: 2     26  -   3
     *   Player 2: A     27  -   4
     *   Player 1: 4     31  -   2 + 4
     *
     * @param onTable the current cards played, and active, this round
     * @param played the current card played by the user
     * @return the score based on any found runs currently on the table
     */
    protected int countRunsOnTable(ArrayList<Card> onTable, Card played) {
        int     count = 0,
                points = 0 ;

        // We can only have runs of 3 or more
        if ( onTable.size() > 1 ) {

            Log.Debug(String.format("  There are enough cards on table (%d) to check for a run.",onTable.size()));

            // Let's add our currently played card to the ones on Table.
            ArrayList<Card> allCards = (ArrayList<Card>)onTable.clone();
            allCards.add(played);

            // Reverse the played cards so we can walk backwards
            Collections.reverse(allCards);

            // Let's get a list of ordered cards
            ArrayList<Card> orderedCards = new ArrayList<Card>();

            // Walk backwards, building the list we'll check for runs
            //   Stop checking when we hit a duplicate card
            for ( Card c : allCards ) {

                // Did we find a duplicate for the current card we are checkin?
                boolean duplicate = false ;

                // Run through the newly created list looking for a duplicate
                for ( Card duplicateCardCheck : orderedCards ) {
                    if ( c.getFace() == duplicateCardCheck.getFace() ) {
                        Log.Debug("  Resetting the run because we found a duplicate.");
                        duplicate = true ;
                        break;
                    }
                }

                // If there is a duplicate, stop adding as this broke the run
                if ( duplicate ) {
                    Log.Debug("  Duplicate truncated our search.");
                    break;
                }

                // Add the current card to the final list
                orderedCards.add(c);

            }

            Log.Debug(String.format("  Final run list before sorting: %s", orderedCards));

            // Sort the list from A to K
            Collections.sort(orderedCards);

            Log.Debug(String.format("  Final run list after sorting: %s", orderedCards));

            // Now, we've created a pure list, one that we can walk through to verify
            //   if the cards constitute a run.  If the currently played card is part
            //   of the run, they score points.  If not, even if there is a run in the
            //   cards, the user will NOT score points.
            for( Card c : orderedCards ) {

                int left = -1 ;

                // In the sorted card list, check the runs
                for ( Card sc : orderedCards ) {

                    // Is this the first card
                    if (left == -1) {

                        left = sc.getFace().ordinal();
                        count = 1;

                        // Are the cards within a single ordinal?
                    } else if (sc.getFace().ordinal()-left == 1) {

                        left = sc.getFace().ordinal();
                        count++;

                        // The sequence is broken...bail
                    } else {

                        // Now, reset the left
                        left = sc.getFace().ordinal();
                        count = 1;
                        Log.Debug(String.format("  Reset the ordered list with card: %s", showCard(sc)));
                        break;
                    }
                }
            }
        } else {
            Log.Debug("  Not enough cards on table to check for a run.");
        }

        Log.Debug(String.format("  Completed scan for runs.  Count of cards in a run is %d.", count));

        // Did we wind up with a run of greater than 2?
        if ( count > 2 )
            points = count ;

        // Log it.
        switch(count){
            case 3: Event.logEvent(EventType.THREE_CARD_SEQUENCE_GAMEPLAY, this); break;
            case 4: Event.logEvent(EventType.FOUR_CARD_SEQUENCE_GAMEPLAY, this);  break;
            case 5: Event.logEvent(EventType.FIVE_CARD_SEQUENCE_GAMEPLAY, this);  break;
            case 6: Event.logEvent(EventType.SIX_CARD_SEQUENCE_GAMEPLAY, this);   break;
            case 7: Event.logEvent(EventType.SEVEN_CARD_SEQUENCE_GAMEPLAY, this); break;
            case 8: Event.logEvent(EventType.EIGHT_CARD_SEQUENCE_GAMEPLAY, this); break;
        }

        Log.Debug(String.format("  %d cards found in sequence.", points));

        return points;
    }

    /**
     * This method counts the number of pairs on the table, and returns the number of
     *   points rewarded to the player.
     * @param onTable the list of cards currently on the table
     * @param played the card being played by this player
     * @return the total number of points based only on pairs on the table
     */
    private int countPairsOnTable(ArrayList<Card> onTable, Card played) {

        int points = 0 ;

        // This only matters if there are cards on the table...
        if ( onTable.size() > 0 ) {

            Log.Debug(String.format("  There are enough cards (%d) on table to check for pairs.", onTable.size()));

            // Work only with a copy of the table so we don't change anything
            ArrayList<Card> cardsOnTableReversed = (ArrayList<Card>) onTable.clone();
            Collections.reverse(cardsOnTableReversed);

            int same = 0;

            // Walk the reversed list of cards on the table and compare each to the
            //   played card.  If they match, increment the same counter.
            Iterator<Card> iCard = cardsOnTableReversed.iterator();
            while (iCard.hasNext()) {
                Card c = iCard.next();
                Log.Debug(String.format("  Comparing card %s to %s for pair match.", played, c));
                if (played.getFace() == c.getFace()) {
                    Log.Debug("  Found a match with " + showCard(c));
                    same++;
                } else {
                    Log.Debug("  Matching ended with " + showCard(c));
                    break;
                }
            }

            // We're counting how many cards on the table are the same as the played
            //   card
            switch (same) {
                case 1:
                    Event.logEvent(EventType.PAIR_GAMEPLAY, this);
                    break;
                case 2:
                    Event.logEvent(EventType.PAIR_ROYALE_GAMEPLAY, this);
                    break;
                case 3:
                    Event.logEvent(EventType.DOUBLE_PAIR_ROYALE_GAMEPLAY, this);
                    break;
            }

            // We always score one more than the match (it takes two to tango)
            points = scorePairs(same+1);

        } else {
            Log.Debug("  Not enough cards on table to check for pairs.");
        }
        return points;
    }

    /**
     * Time to count for the possible 15's and 31's on the table.
     * @param onTable the list of cards on the table
     * @param played the last card played by this player (not on table yet)
     * @return points given for 15 or 31
     */
    protected int count15sAnd31sOnTable(ArrayList<Card> onTable, Card played) {

        int points = 0 ;
        int score = 0 ;

        Log.Debug("  Counting 15s and 31s on the table.");

        // Sum up the table of played cards
        for ( Card c : onTable ) {
            points += c.getValue() ;
        }

        Log.Debug(String.format("  There are %d points on the table, and %d points for the played card %s",
                points, played.getValue(),showCard(played)));

        // Let's see what points this card played creates
        points += played.getValue() ;

        switch ( points ) {
            case 15:
                Log.Debug("  With played card, the player scores 15-2.");
                Event.logEvent(EventType.FIFTEEN_TWO_DURING_GAMEPLAY, this);
                score += Game.POINTS_15_2 ;
                break;
            case 31:
                Log.Debug("  With played card, the player scores 31-2.");
                Event.logEvent(EventType.THIRTY_ONE_DURING_GAMEPLAY, this);
                score += Game.POINTS_31 ;
                break;
        }

        return score ;
    }

    /**
     * Retrieves the largest value card from the Player's hand
     *
     * @return (Card) Largest value triplet in the hand, null if there isn't one.
     */
    public Card getLargestValuedCard() {
        return getHand().stream().max(Comparator.comparing(Card::getValue)).get();
    }

    /**
     * Builds a String that describes the list of cards with a title
     *
     * @param name - the title to display with the list
     * @param list - the list of 'choice' cards to display
     * @return (String) the string describing the card choices
     */
    protected String displayChoices(String name, ArrayList<Choice> list) {

        StringBuilder strCards = new StringBuilder();
        strCards.append(String.format("%s (%d):\n  [", name, list.size()));
        if ( list.isEmpty() ) {
            strCards.append("<empty>");
        } else {
            list.stream().forEach(c -> strCards.append(c.getCard()).append(", "));
            strCards.delete(strCards.length()-2,strCards.length());
        }
        return strCards.append("]").toString() ;
    }

    /**
     * Constructs a String that represents the list of cards in either number
     * or card form.
     *
     * @param asCard - True if the string should show Card representation, values otherwise
     * @param name   - The name to show with the list
     * @param list   - The list of cards to display
     * @return (String) the string representing the list of cards
     */
    static protected String displayCards(Boolean asCard, String name, ArrayList<Card> list) {
        StringBuilder strCards = new StringBuilder();
        strCards.append(String.format("%s (%d):\n", name, list.size()));
        if (list.isEmpty()) {
            strCards.append("      [<empty>]\n");
        } else {
            if (asCard) {
                String rows[] = new String[5];
                int r;
                for (int i = 0; i < 5; i++)
                    rows[i] = "      ";
                for (Card card : list) {
                    card.displayCard(rows);
                }
                for (String str : rows)
                    strCards.append(str + "\n");
            } else {
                strCards.append("  [");
                for (Card card : list) {
                    if (list.lastIndexOf(card) != list.size() - 1)
                        strCards.append(card).append(", ");
                    else
                        strCards.append(card);
                }
                strCards.append("]\n");
            }
        }
        return strCards.toString();
    }

    /**
     * Builds a single string that represents the card given.
     *
     * @param card - the card to display in String form
     * @return (String) - the display string for the card
     */
    static public String showCard(Card card) {
        StringBuilder strReturn = new StringBuilder(50);
        String[] rows = new String[5];
        for (int i = 0; i < 5; i++)
            rows[i] = "";
        card.displayCard(rows);
        for (String strRow : rows)
            strReturn.append(strRow).append("\n");
        return strReturn.toString();
    }

    /**
     * @return string that represents the Player's state
     */
    public String toString() {
        String description = String.format("  Name: %s\n    Starts: %s\n    Score: %d\n",
                name,
                starts ? "yes" : "no",
                score ) +
                displayCards(true, "    Hand", getHand()) +
                displayCards(true, "    Played", getPlayed());
        return description;
    }

    /**
     * We need this for HashMap to work correctly for a Player.
     * The name is the only really relevant field for a player of a game.
     *
     * @return hash of the Player's name
     */
    public int hashCode() {
        return name.hashCode();
    }

    /**
     * We need this for HashMap to work when using a Player as a key
     *
     * @param o - Object to compare this against
     * @return true if names are the same, false otherwise
     */
    public boolean equals(Object o) {
        if ((o instanceof Player) && ((Player) o).getName().equals(name))
            return true;
        return false;
    }

    /**
     * Count the points in the hand with the given start card.
     * @param handToCount the hand to count
     * @param cardStart the start card to count with
     * @return the total points counted in the hand
     */
    public int countHand(ArrayList<Card> handToCount, Card cardStart) {

        ArrayList<Card> handWithStart = (ArrayList<Card>)handToCount.clone();
        handWithStart.add(cardStart);

        // Get the hand in left to right ascending order for all counting
        sortHandByOrdinal(handWithStart);

        int pointsTemp ;
        int pointsInHand = 0 ;

        // Run through the possible points that can be accumulated in a hand
        pointsTemp = countPairs(handWithStart);
        if ( pointsTemp > 0 )
            Log.Info(String.format("  Player '%s' has %d points for pairs.", getName(), pointsTemp));
        pointsInHand += pointsTemp;

        pointsTemp = count15s(handWithStart);
        if ( pointsTemp > 0 )
            Log.Info(String.format("  Player '%s' has %d points for 15's.", getName(), pointsTemp));
        pointsInHand += pointsTemp;

        pointsTemp = countRuns(handWithStart);
        if ( pointsTemp > 0 )
            Log.Info(String.format("  Player '%s' has %d points for runs.", getName(), pointsTemp));
        pointsInHand += pointsTemp;

        pointsTemp = countFlush(handToCount,cardStart);
        if ( pointsTemp > 0 )
            Log.Info(String.format("  Player '%s' has %d points for flush.", getName(), pointsTemp));
        pointsInHand += pointsTemp;

        pointsTemp = countOneForHisNobs(cardStart);
        if ( pointsTemp > 0 )
            Log.Info(String.format("  Player '%s' has %d points for His Knobs.", getName(), pointsTemp));
        pointsInHand += pointsTemp;

        if (pointsInHand == 0 )
            Log.Info(String.format("  Player '%s' has a hand with zero(0) points!", getName()));

        return pointsInHand ;
    }

    /**
     * Count all the pairs present in this list of cards.
     * @param handWithStart the player's hand with the start card added
     * @return points total found in hand attributed to pairs
     */
    protected int countPairs(ArrayList<Card> handWithStart) {

        int score = 0 ;
        int countOfAlikeCards ;

        ArrayList<Face> pairsCounted = new ArrayList<Face>();

        ArrayList<Card> handReverse = new ArrayList<Card>();

        handReverse.addAll(handWithStart);
        Collections.reverse(handReverse);

        for (Card c : handWithStart ) {

            handReverse.remove(handReverse.size()-1);

            // If we've already processed a set of pairs, skip them
            if ( pairsCounted.contains(c.getFace()))
                continue ;

            // Make sure we start with no matches
            countOfAlikeCards = 0 ;

            // Walk the remaining cards looking for any pairs and counting them
            for (Card cr : handReverse) {
                if ( c.getFace() == cr.getFace() ) {
                    countOfAlikeCards++;
                }
            }

            // If we found a pair this time, and we haven't counted it...add it
            if ( countOfAlikeCards > 0  && !pairsCounted.contains(c.getFace()))
                pairsCounted.add(c.getFace());

            // We're counting how many cards on the table are the same as the played
            //   card
            switch (countOfAlikeCards) {
                case 1:
                    Event.logEvent(EventType.PAIR, this);
                    break;
                case 2:
                    Event.logEvent(EventType.PAIR_ROYALE, this);
                    break;
                case 3:
                    Event.logEvent(EventType.DOUBLE_PAIR_ROYALE, this);
                    break;
            }

            score += scorePairs(++countOfAlikeCards);
        }
        return score;
    }

    private int scorePairs(int cardsAlike) {
        int score = 0 ;
        // Update the score based on this counting of alike cards (we add one for the original)
        switch (cardsAlike) {
            case 2: // Two of a kind
                score += Game.POINTS_A_PAIR;
                break;
            case 3: // Three of a kind
                score += Game.POINTS_A_PAIR_ROYAL;
                break;
            case 4: // Four of a kind
                score += Game.POINTS_A_DOUBLE_PAIR_ROYAL;
                break;
        }
        return score ;
    }

    /**
     * Starting with six (6) cards at a time, working down to two (2), count the value of each set determining if the
     *   set adds up to 15 points, with each one that does adding two (2) points to the total score.
     * @param hand - the hand to count 15-2's with
     * @return - how many total points of 15-2's found in the hand
     */
    protected int count15s( ArrayList<Card> hand ) {
        int pointsInHand = 0 ;
        for ( int cardCount = hand.size(); cardCount > 1; cardCount--) {
            pointsInHand += count15s(hand, new ArrayList<Card>(), cardCount, new ArrayList<ArrayList<Card>>());
        }
        return pointsInHand ;
    }

    /**
     * Recursive method to build a list of possible sets of cards composed of 'chooseCount' cards.  The value of each
     *   set of Cards is counted to determine if they add up to 15 or not.  This continues until all possible sets of
     *   cards are counted and accumulated in the return value.
     * @param inputList the list of cards to count
     * @param combo the combination
     * @param chooseCount how many cards in each set to be counted
     * @param resultList the list of sets to be considered
     * @return total points for all 15-2's sets found
     */
    protected int count15s( ArrayList<Card> inputList,
                            ArrayList<Card> combo,
                            int chooseCount,
                            ArrayList<ArrayList<Card>> resultList) {

        int score = 0;

        // If the chooseCount is down to zero (0) that means that we have constructed a set.  Add this set to the
        //   result list so we can count it to see if all the card values add up to 15.
        if (chooseCount == 0) {

            Log.Debug( "  Built combo: " + combo );
            resultList.add(new ArrayList<Card>(combo));

        } else {

            // Let's move through the provided input list adding elements from the input list provided
            for (int i = 0; i < inputList.size(); i++) {

                // start with a brand new list of Cards
                ArrayList<Card> comboList = new ArrayList<>(combo);

                // add each card from the input list to a new combo list
                comboList.add(inputList.get(i));

                // for each set of cards sent in, call this method again with both a smaller input list, a new combo
                // list, and one less count of cards in each set.
                score += count15s( new ArrayList<Card>( inputList.subList(i + 1, inputList.size())),
                        new ArrayList<Card>(comboList),chooseCount - 1, resultList);
            }

            // Now that the combination list provided is empty, we are ready to do the counting of each possible
            //   set of cards created from the original input set, each set composed of the number of cards originally
            //   provided.
            if (combo.isEmpty()) {

                // Use a lambda to log the list of possible 15-2's created from the original list
                resultList.stream().forEach(list -> Log.Debug(String.format("Possible 15-2's with %d cards: ",
                        chooseCount ) + list.toString()));

                Log.Debug(String.format("  15-2's with %d cards: ", chooseCount));

                int count = 0 ;

                // Walk through each set in the result list, add the card's values, and see if they add up to 15
                for (ArrayList<Card> possible15 : resultList ) {
                    if ( possible15.stream().map((c) -> c.getValue()).reduce(0, (accumulator, _item) -> accumulator + _item ) == 15 ) {
                        Log.Debug(possible15 + ": 15-2") ;
                        score += Game.POINTS_15_2;
                        Event.logEvent(EventType.FIFTEEN_TWO,this);
                    }
                }
            }
        }

        // return the total score we found
        return score;
    }

    /**
     * Sorts the hand by the ordinal of the card's face (A - K)
     * @param hand the hand that needs to be sorted
     */
    protected void sortHandByOrdinal(ArrayList<Card> hand) {
        hand.sort(Comparator.comparing(obj -> obj.getFace().ordinal()));
    }

    /**
     * Counts the number of points in runs given the hand with the start card
     * @param handWithStart the player's hand, including the start card
     * @return the number of points in runs in the hand
     */
    protected int countRuns(ArrayList<Card> handWithStart) {

        // sort the hand by ordinal so we can determine a sequential run
        sortHandByOrdinal(handWithStart);

        ArrayList<Card> combo = new ArrayList<Card>();
        ArrayList<ArrayList<Card>> result = new ArrayList<ArrayList<Card>>();

        int pointsInHand = 0 ;
        for ( int cardsInRun = handWithStart.size(); cardsInRun > 2; cardsInRun-- ) {
            pointsInHand = countRuns(handWithStart,combo,cardsInRun,result);
            if ( pointsInHand != 0 )
                break;
            result.clear();
        }
        return pointsInHand ;
    }

    /**
     * The recursive counter of runs, walking through the longest to the shortest runs in the hand
     * @param hand the hand to count runs in
     * @param combo the combination list
     * @param cardsInRun how many cards are in the run
     * @param result the list of results to check
     * @return score of all the valid runs found in the hand
     */
    protected int countRuns(ArrayList<Card> hand, ArrayList<Card> combo, int cardsInRun, ArrayList<ArrayList<Card>> result) {

        int score = 0;

        if (cardsInRun == 0) {

            Log.Debug( "  Built combo: " + combo );
            result.add(new ArrayList<Card>(combo));

        } else {

            // Let's move through the provided input list adding elements.
            for (int i = 0; i < hand.size(); i++) {

                ArrayList<Card> comboList = new ArrayList<>(combo);
                comboList.add(hand.get(i));

                score += countRuns( new ArrayList<Card>( hand.subList(i + 1, hand.size())), new ArrayList<Card>(comboList),cardsInRun - 1, result);
            }

            // Finally print once all combinations are done
            if (combo.isEmpty()) {

                result.stream().forEach(list -> Log.Debug(String.format("Possible runs's with %d cards: ", cardsInRun ) + list.toString()));

                Log.Debug(String.format("  Possible runs with %d cards: ", cardsInRun));

                // The result set contains the list of possible runs
                for (ArrayList<Card> possibleRun : result ) {

                    int lastValue = -1 ;
                    int count = 0 ;
                    boolean areSequential = true ;

                    // Walk the walk, checking one after another...
                    for ( Card c : possibleRun ) {

                        // First time through
                        if ( lastValue == -1 ) {
                            count++;
                            lastValue = c.getFace().ordinal();
                        } else {

                            // Are we walking the walk?
                            if (c.getFace().ordinal() == lastValue + 1) {
                                lastValue = c.getFace().ordinal() ;
                                count++;

                            } else {

                                // If we've hit a roadblock, we don't have a sequence
                                if ( count != cardsInRun ) {
                                    areSequential = false;
                                } else {
                                    break;
                                }
                            }
                        }
                    }

                    if ( areSequential ) {
                        Log.Info(String.format("  Player '%s' scores %d points for run of %d cards in %s.",
                                getName(), cardsInRun, cardsInRun, possibleRun));
                        score += cardsInRun;

                        // Log the run
                        switch(cardsInRun){
                            case 3: Event.logEvent(EventType.THREE_CARD_SEQUENCE, this); break;
                            case 4: Event.logEvent(EventType.FOUR_CARD_SEQUENCE, this);  break;
                            case 5: Event.logEvent(EventType.FIVE_CARD_SEQUENCE, this);  break;
                            case 6: Event.logEvent(EventType.SIX_CARD_SEQUENCE, this);   break;
                            case 7: Event.logEvent(EventType.SEVEN_CARD_SEQUENCE, this); break;
                            case 8: Event.logEvent(EventType.EIGHT_CARD_SEQUENCE, this); break;
                        }

                    }
                }
            }
        }
        return score;
    }

    protected int countFlush(ArrayList<Card> hand, Card cardStart) {
        int score = 0 ;

        Map<Suit,Integer> map = new HashMap<Suit,Integer>(){{
            put(Suit.CLUBS,0);
            put(Suit.DIAMONDS,0);
            put(Suit.HEARTS,0);
            put(Suit.SPADES,0);
        }};

        for (Card c : hand ) {
            int count = map.get(c.getSuit());
            map.replace(c.getSuit(),++count);
        }

        for(Map.Entry<Suit, Integer> entry:map.entrySet()) {
            if ( entry.getValue() == hand.size() ) {
                if ( entry.getKey() == cardStart.getSuit() ) {
                    score += hand.size() + 1;
                    Event.logEvent(EventType.FIVE_CARD_FLUSH, this);
                    Log.Info(String.format("  Player '%s' scores %d points for 'Flush with Start Card'.", getName(), score));
                } else {
                    score += hand.size();
                    Event.logEvent(EventType.FOUR_CARD_FLUSH, this);
                    Log.Info(String.format("  Player '%s' scores %d points for 'Flush'.", getName(), score));
                }
            }
            Log.Debug(entry.getKey()+ " : "+entry.getValue());
        }

        return score;
    }

    protected int countOneForHisNobs(Card cardStart){
        int score = 0 ;
        for ( Card c : getHand() ) {
            if ( c.getFace() == Face.JACK ) {
                if ( c.getSuit() == cardStart.getSuit() ) {
                    Log.Info( String.format( "  Player '%s' scores for 'His Nobs'.", getName() ));
                    Event.logEvent(EventType.HIS_NOBS,this);
                    score += 1 ;
                }
            }
        }
        return score; }

}