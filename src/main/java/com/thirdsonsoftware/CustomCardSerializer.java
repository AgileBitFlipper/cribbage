package com.thirdsonsoftware;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class CustomCardSerializer extends StdSerializer<Card> {

    public CustomCardSerializer() {
        this(null);
    }

    public CustomCardSerializer(Class<Card> c) {
        super(c);
    }

    @Override
    public void serialize(Card card, JsonGenerator jsonGenerator, SerializerProvider serializer) {
        try {
            jsonGenerator.writeStartObject();

            jsonGenerator.writeNumberField("id", card.getId());
            jsonGenerator.writeStringField("suit", card.getSuit().toString());
            jsonGenerator.writeStringField("face", card.getFace().toString());
            jsonGenerator.writeNumberField("value", card.getFace().getValue());
            //jsonGenerator.writeBooleanField("placed", card.getCardsAlreadyPlayed());
            jsonGenerator.writeBooleanField("tray", card.getInHand());
            //jsonGenerator.writeStringField("deck", card.getInDeck().toString());
            jsonGenerator.writeBooleanField("bUseColors", card.bUseColors);

            jsonGenerator.writeEndObject();
        } catch (IOException ioe) {
            Log.Error("  IOException serializing the Player object: " + ioe.getMessage());
        }
    }
}
