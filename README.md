# README #

Welcome to "cribbage", a project designed to provide both a game engine and analysis for
playing six-card cribbage.

# Gameplay #

Cribbage is a popular card game dating back so far that origins are hard to determine.  But, we
are going to be persuing not just producing an automated gameplay engine, but producing output that
can be analyzed to improve a player's ability to score.

### How do I get set up? ###

* Summary of set up

  Clone the repository locally (the design branch is bleeding edge, while develop is last pull).  
  Open IntelliJ IDEA and point it at the project paht.  Open the Main.java file and run the 
  application.

* Configuration

  Get the carddeck JAR file from the carddeck repository in my account.  This JAR file provides the 
  necessary functionality of a deck of cards that is needed for this application.

* Run the application
  * Use "**java -jar cribbage.jar**"  
      d - Debug logging    
      g # - Number of games to play    
      p # - Number of players in the game    
      a - Analyze the events recorded    

* Dependencies  
    * ant for building  
    * ant-launcher for building  
    * commons-cli for command line processing  
    * commons-logging for logging  
    * groovy-all for testing  
    * junit for testing  
    * jackson-annotations, jackson-core, and jackson-databind for JSON event files
    * jline

* How to run tests

  Use IntelliJ IDEA to launch the configuration for com.thirdsonsoftware.PlayerTest class or 
  com.thirdsonsoftware.GameTest.  Additional tests to follow.  IntelliJ offers a nice way to run
  all tests at a single time.  It does require a proper runner, and this is part of the dependencies
  that I've already included in the project.

* Deployment instructions

  Put the cribbage.jar file in the class path and use:
  java -jar cribbage.jar <options> to execute.

### Contribution guidelines ###

* Writing Tests 

  Testing is an extremely important part of assuring the reliability of any application.  Thus, any 
  and all contributions should be well written, sufficiently commented, and include tests for any new 
  code or functionality.  You must keep the code coverage to a maximum for any new code.
* Code Review 

  Code reviews are mandatory (and always recommended) as this is how we learn and grow.  It should be 
  an enjoyable process and not dreaded.  If you aren't having fun, you aren't doing it right.
* Other Guidelines

  Like I said, you should be having fun with this project.  I have so far, and I'd love to share it 
  with you.  Drop me a line or two if you have questions or would like to chat!

### Who do I talk to? ###

* Repo owner: Andrew B. Montcrieff
* Contact: 
  * AgileBitFlipper (Twitter)
  * amontcri@yahoo.com